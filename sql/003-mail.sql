
USE [lin2world]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[post](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[create_date] [datetime] NOT NULL DEFAULT (GETUTCDATE()),
	[expire_date] [datetime] NOT NULL DEFAULT (GETUTCDATE()),
    [sender_id] [int] NOT NULL,
    [sender_deleted] [int] NOT NULL DEFAULT 0,
	[sender_name] [nvarchar](25) NOT NULL,
	[recipient_id] [int] NOT NULL,
	[recipient_name] [nvarchar](25) NOT NULL,
    [recipient_deleted] [int] NOT NULL DEFAULT 0,
	[subject] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[content] [nvarchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[cod_adena] [int] NOT NULL DEFAULT 0,
    [read] [int] NOT NULL DEFAULT 0,
    [item1] [int] NOT NULL DEFAULT 0,
    [item2] [int] NOT NULL DEFAULT 0,
    [item3] [int] NOT NULL DEFAULT 0,
    [item4] [int] NOT NULL DEFAULT 0,
    [item5] [int] NOT NULL DEFAULT 0,
    [item6] [int] NOT NULL DEFAULT 0,
    [item7] [int] NOT NULL DEFAULT 0,
    [item8] [int] NOT NULL DEFAULT 0,
    [expired] [int] NOT NULL DEFAULT 0
 CONSTRAINT [PK_post] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

