
#include <Server/COlympiadDefaultSetting.h>
#include <Common/Utils.h>

const double COlympiadDefaultSetting::Three = 3.0;

void COlympiadDefaultSetting::Init()
{
	WriteInstructionCall(0x76D90F, reinterpret_cast<UINT32>(GetTransferingPointWrapper));
	WriteInstructionCall(0x76DFB9, reinterpret_cast<UINT32>(GetPenaltyPointWrapper));
	WriteInstructionCall(0x76DFDD, reinterpret_cast<UINT32>(GetPenaltyPointWrapper));
	WriteInstructionCall(0x76E451, reinterpret_cast<UINT32>(GetPenaltyPointWrapper));
	WriteAddress(0x768FAC, reinterpret_cast<UINT32>(&Three));
	WriteInstructionCall(0x774CC5, reinterpret_cast<UINT32>(fixed_wcsncmp));
}

int COlympiadDefaultSetting::GetTransferingPointWrapper(COlympiadDefaultSetting *self, int a, int b, int c)
{ GUARDED

	return self->GetTransferingPoint(a, b, c);
}

int COlympiadDefaultSetting::GetTransferingPoint(int a, int b, int c)
{ GUARDED

	int result = reinterpret_cast<int(*)(COlympiadDefaultSetting*, int, int, int)>(0x768E48)(this, a, b, c);
	return result < 10 ? result : 10;
}

int COlympiadDefaultSetting::GetPenaltyPointWrapper(COlympiadDefaultSetting *self, int a, int b)
{ GUARDED

	return self->GetPenaltyPoint(a, b);
}

int COlympiadDefaultSetting::GetPenaltyPoint(int a, int b)
{ GUARDED

	int result = reinterpret_cast<int(*)(COlympiadDefaultSetting*, int, int)>(0x768F78)(this, a, b);
	return result < 10 ? result : 10;
}

int COlympiadDefaultSetting::fixed_wcsncmp(const wchar_t *first, const wchar_t *second, size_t count)
{
	if (!first) return 1;
	return wcsncmp(first, second, count);
}