
#pragma once

class MailSystem {
public:
	static bool RequestPostItemListPacket(class CUserSocket *socket, const unsigned char *packet);
	static bool RequestSendPostPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplySendPostRecipientNotFound(class CUser *user);
	static void ReplySendPostRecipientIsSender(class CUser *user);
	static void ReplySendPostRecipientBlockedSender(class CUser *user);
	static void ReplySendPostRecipientMailboxFull(class CUser *user);
	static void ReplySendPostNotEnoughAdena(class CUser *user);
	static void ReplySendPostDone(class CUser *sender, class CUser *recipient, const unsigned char *data);
	static bool RequestReceivePostListPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplyReceivePostList(class CUser *user, const unsigned char *data);
	static bool RequestDeleteReceivedPostPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplyDeleteReceivedPost(class CUser *user, const unsigned char *data);
	static bool RequestReceivedPostPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplyReceivedPost(class CUser *user, const unsigned char *data);
	static bool RequestReceivePostPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplyReceivePost(class CUser *user, const unsigned char *data);
	static bool RequestRejectPostPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplyRejectPost(class CUser *user, const unsigned char *data);
	static bool RequestSentPostListPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplySentPostList(class CUser *user, const unsigned char *data);
	static bool RequestDeleteSentPostListPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplyDeleteSentPost(class CUser *user, const unsigned char *data);
	static bool RequestSentPostPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplySentPost(class CUser *user, const unsigned char *data);
	static bool RequestCancelSentPostPacket(class CUserSocket *socket, const unsigned char *packet);
	static void ReplyCancelSentPost(CUser *user, const unsigned char *data);
	static void ReplyCheckMail(class CUser *user, const unsigned char *data);
	static void Upkeep();
	static void ReplyReturnedPost(const unsigned char *data);
};

