
#include <Server/CSkillOperateCondition_op_skill_acquire.h>
#include <Server/CCreature.h>

CSkillOperateCondition_op_skill_acquire::CSkillOperateCondition_op_skill_acquire(UINT32 skillId, bool learned) : skillId(skillId), learned(learned)
{
}

bool CSkillOperateCondition_op_skill_acquire::CanUse(CCreature *caster, const class CSkillInfo *skillInfo, class CObject *target)
{
	(void) skillInfo;
	(void) target;

	if (!target) return false;
	CCreature *targetCreature = reinterpret_cast<CCreature*>(target);
	if (!targetCreature->IsValidCreature()) return false;
	return learned == (targetCreature->GetAcquiredSkillLevel(skillId) > 0);
}

