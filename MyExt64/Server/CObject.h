
#pragma once

#include <windows.h>

class CObject {
public:
	static CObject* GetObject(const UINT32 objectId);

	template<class T>
	T* Copy()
	{
		return GetVfn<T*(*)(CObject*)>(this, 0x1E)(this);
	}

	void Delete();

	int GetDBID();
	void SetDBID(const int id);
	bool IsItem();
};

