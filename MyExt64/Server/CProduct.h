
#pragma once

#include <vector>
#include <windows.h>

class CProduct {
public:
	class ItemData {
	public:
		int itemId;
		int count;
	};

	/* 0x0000 */ unsigned char padding0x0000[0x0258 - 0x0000];
	/* 0x0258 */ unsigned int price;
	/* 0x025C */ unsigned char padding0x025C[0x0462 - 0x025C];
	/* 0x0462 */ SYSTEMTIME start;
	/* 0x0472 */ SYSTEMTIME end;
	/* 0x0482 */ unsigned char padding0x0482[0x0690 - 0x0482];
	/* 0x0690 */ std::vector<ItemData> data;
};

