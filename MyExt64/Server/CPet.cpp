
#include <Server/CPet.h>
#include <Server/CUser.h>
#include <Server/NpcServer.h>
#include <Server/CItem.h>
#include <Common/CSharedCreatureData.h>
#include <Common/Utils.h>
#include <Common/Config.h>

void CPet::Init()
{
	WriteMemoryQWORD(0xBCCD98, reinterpret_cast<UINT64>(ExpIncWrapper));
	if (Config::Instance()->custom->dontDropPetItems) {
		NOPMemory(0x7B6079, 2);
	}
	WriteInstructionCall(0x7B5F99, reinterpret_cast<UINT32>(GiveAllItemToMasterHelper));
	WriteMemoryBYTES(0x7B5F9E, "\x85\xC0\x74\x60", 4); // test eax, eax; je 0x7B6002
	WriteInstructionCall(0x5D33CC + 0x373, reinterpret_cast<UINT32>(GiveAllItemToMasterWrapper));
	WriteInstructionCall(0x8FCF50 + 0x184, reinterpret_cast<UINT32>(GiveAllItemToMasterWrapper));
	WriteInstructionCall(0x8FDB18 + 0x1A8, reinterpret_cast<UINT32>(GiveAllItemToMasterWrapper));
	if (Config::Instance()->server->epiloguePetInventoryBehaviour) {
		NOPMemory(0x8FD0D4, 5);
	}

	WriteMemoryBYTES(0x4449A5, "\x48\x89\xF1\x4C\x89\xE2", 6);
	WriteInstructionCallJmpEax(0x4449AB, FnPtr(AtomicDestroyPetDoHelper));
	WriteInstructionCall(0x444A28, FnPtr(AtomicDestroyPetDoCDBDeletePet));
}

INT64 __cdecl CPet::ExpIncWrapper(CPet *self, const INT64 exp, const bool b)
{
	return self->ExpInc(exp, b);
}

INT64 CPet::ExpInc(const INT64 exp, const bool b)
{
	GUARDED;

	bool isExpOff = false;
	CUser *user = GetUserOrMaster();
	if (user && user->ext.isPetExpOff) {
		isExpOff = true;
	}

	return reinterpret_cast<INT64(*)(CPet*, const INT64, const bool)>(0x7ADC4C)(
        this, (exp < 0 || !isExpOff) ? exp : 0, b);
}

void CPet::GiveAllItemToMasterWrapper(CPet *self, bool b)
{
	self->GiveAllItemToMaster(b);
}

void CPet::GiveAllItemToMaster(bool b)
{
	CUser *user = GetUserOrMaster();
	if (user) {
		user->TradeCancel();
	}
	reinterpret_cast<void(*)(CPet*, bool)>(0x7B5E58)(this, b);
}

UINT32 CPet::GiveAllItemToMasterHelper(CItem *item)
{
	// "bool __cdecl User::Ride(class CPet *)"
	// "bool __cdecl AtomicDestroyPet::Do(void)"
	if (!Guard::WasCalled(reinterpret_cast<wchar_t*>(0xC5FB50))
		&& !Guard::WasCalled(reinterpret_cast<wchar_t*>(0xA62EE0))) {

		switch (Config::Instance()->custom->keepFoodInPetInventory ? item->itemId : 0) {
		case 2515: // wolf food
		case 4038: // food for hatchling
		case 5168: // food for strider
		case 5169: // deluxe food for strider
		case 7582: // baby spice
		case 9668: // great wolf food
		case 10425: // improved baby pet food
		case 14818: // enriched pet food for wolves
			return 0;
		}
	}

	return (*reinterpret_cast<UINT32(***)(CItem*)>(item))[0x8C](item);
}

void CPet::SetLevel(const int level)
{
	reinterpret_cast<void(*)(CPet*, int)>(0x7AFA40)(this, level);
}

void CPet::SetSP(const int sp)
{
	reinterpret_cast<void(*)(CPet*, int)>(0x52C36C)(this, sp);
}

UINT64 CPet::AtomicDestroyPetDoHelper(CUser *user, int *obj)
{
	GUARDED;

	if (user && user->IsUser()) {
		CPet *pet = reinterpret_cast<CPet*>(user->GetSummonOrPet());
		if (pet && pet->IsPet()) {
			pet->GiveAllItemToMaster(false);
			NpcServer::Instance()->Send("cddd", 0x8F, pet->objectId, 0, 0);
			return 0x4449EA;
		}
	}
	CLog::Add(CLog::Red, L"[%s][%d] DestroyPet failed. Pet is summoned. Pet collar DBID[%d]",
		__FILEW__, __LINE__, obj[7]);
	return 0x4449D6;
}

void CPet::AtomicDestroyPetDoCDBDeletePet(void *instance, int a, int b, int c, int d, int e)
{
	GUARDED;

	reinterpret_cast<void(*)(void*, int, int, int, int, int)>(0x5880E4)(instance, a, b, c, d, e);
	reinterpret_cast<void(*)(UINT64, int, int)>(0x7B63D4)(0x10B089C0, b, a);
}
