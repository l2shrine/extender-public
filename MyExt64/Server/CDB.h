
#pragma once

#include <windows.h>
#include <vector>

class CDB {
private:
	CDB();
	CDB(const CDB &other);

public:
	enum {
		REQUEST_TEST = 0,
		REPLY_TEST,
		REQUEST_SEND_POST,
		REPLY_SEND_POST_RECIPIENT_NOT_FOUND,
		REPLY_SEND_POST_RECIPIENT_IS_SENDER,
		REPLY_SEND_POST_RECIPIENT_BLOCKED_SENDER,
		REPLY_SEND_POST_RECIPIENT_MAILBOX_FULL,
		REPLY_SEND_POST_NOT_ENOUGH_ADENA,
		REPLY_SEND_POST_DONE,
		REQUEST_RECEIVE_POST_LIST,
		REPLY_RECEIVE_POST_LIST,
		REQUEST_SENT_POST_LIST,
		REPLY_SENT_POST_LIST,
		REQUEST_RECEIVED_POST,
		REPLY_RECEIVED_POST,
		REQUEST_SENT_POST,
		REPLY_SENT_POST,
		REQUEST_DELETE_RECEIVED_POST,
		REPLY_DELETE_RECEIVED_POST,
		REQUEST_DELETE_SENT_POST,
		REPLY_DELETE_SENT_POST,
		REQUEST_RECEIVE_POST,
		REPLY_RECEIVE_POST,
		REQUEST_CHECK_MAIL,
		REPLY_CHECK_MAIL,
		REQUEST_REJECT_POST,
		REPLY_REJECT_POST,
		REQUEST_CANCEL_POST,
		REPLY_CANCEL_POST,
		REQUEST_MAIL_UPKEEP,
		REPLY_RETURNED_POST
	};

	static void Init();
	static CDB* Instance();

	/* 0x0000 */ virtual ~CDB();
	/* 0x0008 */ virtual void vfn1() {}
	/* 0x0010 */ virtual void vfn2() {}
	/* 0x0018 */ virtual void vfn3() {}
	/* 0x0020 */ virtual void Send(const char *format, ...) {}

	static void __cdecl SendSaveCharacterInfoWrapper(CDB *self, class CUser *user, bool a, bool b);

	void RequestMarkInZoneRestriction(UINT32 userDbId, UINT32 zoneId, UINT32 time, UINT32 count, bool b, UINT32 i);
	void RequestLoadUserPoint(class CUser *user, int type);
	void SendSaveCharacterInfo(class CUser *user, bool a, bool b);
	void SetDailyQuest(UINT32 charId, UINT32 questId, UINT32 completeTime);
	void RequestBuyItems(UINT32 userDbId, UINT32 npcId, CUser *user, INT64 price, UINT32 tax, void *pledge, UINT32 items, UINT32 dataSize, const char *data);

	static bool ReplyExPacket(class CDBSocket *socket, const unsigned char *packet);
	static bool ReplyExtPacket(class CDBSocket *socket, const unsigned char *packet);

	void RequestTest(int i);
	static bool ReplyTest(class CDBSocket *socket, const unsigned char *packet);

	void RequestSendPost(CUser *user,
                         const wchar_t *recipient,
                         const wchar_t *subject,
                         const wchar_t *content,
                         int attachmentCount,
                         size_t attachmentsSize,
                         const char *attachments,
                         INT64 codAdena);
	static bool ReplySendPostRecipientNotFound(class CDBSocket *socket, const unsigned char *packet);
	static bool ReplySendPostRecipientIsSender(class CDBSocket *socket, const unsigned char *packet);
	static bool ReplySendPostRecipientBlockedSender(class CDBSocket *socket, const unsigned char *packet);
	static bool ReplySendPostRecipientMailboxFull(class CDBSocket *socket, const unsigned char *packet);
	static bool ReplySendPostNotEnoughAdena(class CDBSocket *socket, const unsigned char *packet);
	static bool ReplySendPostDone(class CDBSocket *socket, const unsigned char *packet);

	void RequestReceivePostList(CUser *user);
	static bool ReplyReceivePostList(class CDBSocket *socket, const unsigned char *packet);

	void RequestSentPostList(CUser *user);
	static bool ReplySentPostList(class CDBSocket *socket, const unsigned char *packet);

	void RequestReceivedPost(CUser *user, const int id);
	static bool ReplyReceivedPost(class CDBSocket *socket, const unsigned char *packet);

	void RequestSentPost(CUser *user, const int id);
	static bool ReplySentPost(class CDBSocket *socket, const unsigned char *packet);

	void RequestDeleteReceivedPost(CUser *user, const std::vector<int> &ids);
	static bool ReplyDeleteReceivedPost(class CDBSocket *socket, const unsigned char *packet);

	void RequestDeleteSentPost(CUser *user, const std::vector<int> &ids);
	static bool ReplyDeleteSentPost(class CDBSocket *socket, const unsigned char *packet);

	void RequestReceivePost(CUser *user, const int id);
	static bool ReplyReceivePost(class CDBSocket *socket, const unsigned char *packet);

	void RequestCheckMail(CUser *user);
	static bool ReplyCheckMail(class CDBSocket *socket, const unsigned char *packet);

	void RequestRejectPost(CUser *user, const int id);
	static bool ReplyRejectPost(class CDBSocket *socket, const unsigned char *packet);

	void RequestCancelPost(CUser *user, const int id);
	static bool ReplyCancelPost(class CDBSocket *socket, const unsigned char *packet);

	void RequestMailUpkeep();
	static bool ReplyReturnedPost(class CDBSocket *socket, const unsigned char *packet);
};

