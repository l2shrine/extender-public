
#include <Server/NpcServer.h>
#include <Server/CCreature.h>
#include <Server/CNPC.h>
#include <Server/CUser.h>
#include <Server/CUserSocket.h>
#include <Server/GraciaEpilogue.h>
#include <Server/CPet.h>
#include <Server/CParty.h>
#include <Server/CContributeData.h>
#include <NPCd/NPCd.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <Common/CSharedCreatureData.h>
#include <Common/SmartPtr.h>
#include <NPCd/ExtPacket.h>
#include <set>

void NpcServer::Init()
{
	WriteMemoryBYTES(0x74B579, "\x48\x8B\x94\x24\xA0\x00\x00\x00\x48\x8B\x8C\x24\xB0\x42\x00\x00", 16);
	WriteInstructionCall(0x74B589, reinterpret_cast<UINT32>(SendCreateOnePrivateNearUserWrapper), 0x74B591);
	WriteAddress(0xA2742B, reinterpret_cast<UINT32>(NpcEx));
	WriteAddress(0xA28033, reinterpret_cast<UINT32>(NpcSetParam));
	WriteInstructionJmp(0x74BC70, FnPtr(FindRandomUser));
}

NpcServer::NpcServer()
{
}

NpcServer::NpcServer(const NpcServer &other)
{
}

NpcServer::~NpcServer()
{
}

NpcServer* NpcServer::Instance()
{
	return reinterpret_cast<NpcServer*>(0x1634170);
}

void* NpcServer::GetSocket()
{
	return reinterpret_cast<void*(*)(void*)>(0x72CD10)(this);
}

void NpcServer::Send(const char *format, ...)
{
	GUARDED;

	va_list va;
	va_start(va, format);
	SendV(format, va);
	va_end(va);
}

void NpcServer::SendV(const char *format, va_list va)
{
	void *socket = GetSocket();
	if (!socket) {
		CLog::Add(CLog::Red, L"Attempt to write to null socket!");
		return;
	}
	reinterpret_cast<void(*)(void*, const char*, va_list)>(0x859934)(socket, format, va);
}

void __cdecl NpcServer::SendCreateOnePrivateNearUserWrapper(void *self, CCreature *creature, BYTE unused, UINT32 a, UINT32 b, UINT64 c, UINT32 d, UINT32 e, UINT32 f, UINT32 g, UINT32 h, UINT32 i, UINT32 j, UINT32 k, wchar_t *l, UINT32 m, UINT32 n, void *o, UINT32 p)
{
	Instance()->Send("cddpddddddddSdbdd", 0x98, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, creature->sd->inZoneId);
}

bool NpcServer::NpcSetParam(void *socket, const unsigned char *bytes)
{
	UINT32 objectId;
	UINT32 param;
	double value;
	Disassemble(bytes, "ddf", &objectId, &param, &value);
	if (param == 9 || param == 35) {
		return reinterpret_cast<bool(*)(void*, const unsigned char*)>(0x734C04)(socket, bytes);
	}
	if (!objectId) {
		return 0;
	}
	CPet *pet = reinterpret_cast<CPet*>(CObject::GetObject(objectId));
	if (!pet || !pet->IsPet()) {
		return 0;
	}
	switch (param) {
	case 1: // level
		pet->sdLock->Enter(__FILEW__, __LINE__);
		pet->SetLevel(int(value));
		pet->sdLock->Leave(__FILEW__, __LINE__);
		break;
	case 13: // SP
		pet->sdLock->Enter(__FILEW__, __LINE__);
		pet->SetSP(int(value));
		pet->sdLock->Leave(__FILEW__, __LINE__);
		break;
	default:
		break;
	}
	return false;
}

bool NpcServer::FindRandomUser(void *socket, const unsigned char *bytes)
{
	int npc = 0;
	Disassemble(bytes, "d", &npc);

	int tick = GetTickCount();

	ScopedLock lock(CUser::onlineOfflineTradeUsersCS);

	std::set<CUser*> users;
	std::vector<CUser*> usersVector;

	for (std::set<CUser*>::iterator i = CUser::onlineUsers.begin() ; i != CUser::onlineUsers.end() ; ++i) {
		CUser *user = *i;
		if (!user->sd) continue;
		if (user->hide) continue;
		if (!user->sd->isAlive) continue;
		if (user->sd->isInsidePeaceZone) continue;
		if (user->sd->inZoneId) continue;
		if (user->ext.lastActionTick < tick - 60000) continue;
		if (user->IsInOlympiad()) continue;
		CParty *party = user->GetParty();
		if (party) {
			xvector<CUser*> partyUsers;
			party->GetAllMember(&partyUsers);
			for (xvector<CUser*>::iterator j = partyUsers.begin() ; j != partyUsers.end() ; ++j) {
				if (!(*j)->sd) continue;
				if ((*j)->hide) continue;
				if (!(*j)->sd->isAlive) continue;
				if ((*j)->sd->isInsidePeaceZone) continue;
				if ((*j)->sd->inZoneId) continue;
				if ((*j)->IsInOlympiad()) continue;
				if (!users.insert(*j).second) continue;
				usersVector.push_back(*j);
			}
		} else if (users.insert(*i).second) {
			usersVector.push_back(*i);
		}
	}

	if (usersVector.empty()) {
		return false;
	}

	size_t n = rand() % usersVector.size();

	int objectId = usersVector[n]->objectId;
	int index = usersVector[n]->sd->index;

	lock.Release();

	NpcServer::Instance()->Send("cddd", 0x97, npc, objectId, index);

	return false;
}

bool NpcServer::NpcEx(void *socket, const unsigned char *bytes)
{
	const unsigned char *data = &bytes[2];
	switch (*reinterpret_cast<const UINT16*>(bytes)) {
	case NPCd::WHISPER: return NpcWhisper(socket, data);
	case NPCd::SET_ABILITY_ITEM_DROP: return NpcSetAbilityItemDrop(socket, data);
	case NPCd::SHOW_BUY_SELL: return NpcShowBuySell(socket, data);
	case NPCd::IS_TOGGLE_SKILL_ONOFF: return NpcIsToggleSkillOnOff(socket, data);
	case NPCd::RIDE_WYVERN2: return NpcRideWyvern2(socket, data);
	case NPCd::CLEAR_CONTRIBUTE_DATA: return ClearContributeData(socket, data);
	}
	return true;
}

bool NpcServer::NpcWhisper(void *socket, const unsigned char *bytes)
{
	UINT32 npcIndex;
	UINT32 talkerIndex;
	wchar_t text[1024];

	Disassemble(bytes, "ddS", &npcIndex, &talkerIndex, sizeof(text) / sizeof(text[0]), text);

	SmartPtr<CCreature> npc = SmartPtr<CCreature>::FromIndex(npcIndex);
	SmartPtr<CCreature> talker_ = SmartPtr<CCreature>::FromIndex(talkerIndex);
	if (!npc || !talker_ || !talker_->IsUser()) {
		return false;
	}

	CUser &talker = reinterpret_cast<CUser&>(*talker_);
	CUserSocket *userSocket = talker.socket;
	if (userSocket) {
		userSocket->Send("cdddS", 0x30, npc->objectId, 0, npc->objectType, text);
	}

	return false;
}

bool NpcServer::NpcSetAbilityItemDrop(void *socket, const unsigned char *bytes)
{
	UINT32 npcIndex;
	unsigned char value;
	wchar_t text[1024];

	Disassemble(bytes, "dc", &npcIndex, &value);

	SmartPtr<CCreature> npc = SmartPtr<CCreature>::FromIndex(npcIndex);
	if (npc) {
		if (value) {
			npc->inventory.noDropItems &= 1;
		} else {
			npc->inventory.noDropItems |= 2;
		}
	}

	return false;
}

bool NpcServer::NpcShowBuySell(void *socket, const unsigned char *bytes)
{
	GUARDED;

	UINT32 npcIndex;
	UINT32 talkerIndex;
	UINT32 buyList;
	UINT32 sellList;
	double rate;

	Disassemble(bytes, "ddddf", &npcIndex, &talkerIndex, &buyList, &sellList, &rate);
	unsigned char buyBuffer[32];
	unsigned char sellBuffer[24];
	Assemble(reinterpret_cast<char*>(buyBuffer), sizeof(buyBuffer), "dddSSSf", talkerIndex, npcIndex, buyList, L"", L"", L"", rate);
	Assemble(reinterpret_cast<char*>(sellBuffer), sizeof(sellBuffer), "dddSSS", talkerIndex, npcIndex, sellList, L"", L"", L"");

	typedef bool (__cdecl *t)(void*, const BYTE*);

	SmartPtr<CUser> user = SmartPtr<CUser>::FromIndex(talkerIndex);
	if (!user || !user->IsUser()) {
		return false;
	}

	CUserSocket *userSocket = user->socket;
	if (!userSocket) {
		return false;
	}

	user->sdLock->Enter(__FILEW__, __LINE__);
	GraciaEpilogue::CUserReleaseEconomy(&*user);
	user->sdLock->Leave(__FILEW__, __LINE__);

	if (reinterpret_cast<t>(0x747184)(socket, sellBuffer)) {
		return true;
	}

	user->sdLock->Enter(__FILEW__, __LINE__);

	if (!user->economy) {
		GraciaEpilogue::CUserReleaseEconomy(&*user);
		user->sdLock->Leave(__FILEW__, __LINE__);
		return false;
	}

	CEconomy *tmp = user->economy;
	user->economy = user->ext.buySell.economy2;
	user->ext.buySell.economy2 = tmp;

	user->sdLock->Leave(__FILEW__, __LINE__);

	if (reinterpret_cast<t>(0x746318)(socket, buyBuffer)) {
		user->sdLock->Enter(__FILEW__, __LINE__);
		GraciaEpilogue::CUserReleaseEconomy(&*user);
		user->sdLock->Leave(__FILEW__, __LINE__);
		return true;
	}

	user->sdLock->Enter(__FILEW__, __LINE__);

	if (!user->economy) {
		GraciaEpilogue::CUserReleaseEconomy(&*user);
		user->sdLock->Leave(__FILEW__, __LINE__);
		return false;
	}

	tmp = user->economy;
	user->economy = user->ext.buySell.economy2;
	user->ext.buySell.economy2 = tmp;

	std::pair<int, std::string> refund(user->GetRefundData());

	userSocket->Send("chQdhbhbhbc", 0xFE, 0xB7,
		user->ext.buySell.buyList.adena,
		user->ext.buySell.buyList.id,
		user->ext.buySell.sellList.itemCount,
		user->ext.buySell.sellList.items.size(),
		user->ext.buySell.sellList.items.data(),
		user->ext.buySell.buyList.itemCount,
		user->ext.buySell.buyList.items.size(),
		user->ext.buySell.buyList.items.data(),
		refund.first,
		refund.second.size(),
		refund.second.data(),
		!user->ext.buySell.firstBuySell);

	user->ext.buySell.firstBuySell = false;

	user->sdLock->Leave(__FILEW__, __LINE__);

	return false;
}

bool NpcServer::NpcIsToggleSkillOnOff(void *socket, const unsigned char *bytes)
{
	UINT32 npcIndex;
	UINT32 targetIndex;
	UINT32 skillId;
	Disassemble(bytes, "ddd", &npcIndex, &targetIndex, &skillId);
	SmartPtr<CCreature> npc = SmartPtr<CCreature>::FromIndex(npcIndex);
	SmartPtr<CCreature> target = SmartPtr<CCreature>::FromIndex(targetIndex);
	if (!npc || !target) return false;
	UINT32 status = target->IsToggleSkillOnOff(skillId);
	NpcServer::Instance()->Send("chdddd", 0x9A, ExtPacket::IS_TOGGLE_SKILL_ONOFF,
		npc->objectId, target->objectId, status, skillId);
	return false;
}

bool NpcServer::NpcRideWyvern2(void *socket, const unsigned char *bytes)
{
	UINT32 talkerIndex;
	UINT32 classId;
	UINT32 duration;
	Disassemble(bytes, "ddd", &talkerIndex, &classId, &duration);
	SmartPtr<CCreature> talker = SmartPtr<CCreature>::FromIndex(talkerIndex);
	if (!talker) return false;
	if (!talker->IsUser()) return false;
	CUser *user = reinterpret_cast<CUser*>(&*talker);
	if (user->sd->stopMode != 1) return false;
	if (user->IsNowTrade()) return false;
	if (!user->IsItemUsable()) return false;
	if (user->sd->yongmaType) return false;
	user->RideForEvent(classId, duration, false);
	return false;
}

bool NpcServer::ClearContributeData(void *socket, const unsigned char *bytes)
{
	UINT32 npcIndex;
	Disassemble(bytes, "d", &npcIndex);
	SmartPtr<CCreature> npc = SmartPtr<CCreature>::FromIndex(npcIndex);
	if (npc) {
		npc->contributeData->Clear();
	}
	return false;
}

