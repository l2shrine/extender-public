
#pragma once

#include <Windows.h>

class CSkillKey {
public:
	CSkillKey(const INT16 skillId = 0, const INT16 level = 0);
	INT16 skillId;
	INT16 level;
};

class CSkillDB {
private:
	CSkillDB();
	CSkillDB(const CSkillDB &other);
	CSkillDB& operator=(const CSkillDB &other);

public:
	static CSkillDB* Instance();
	class CSkillInfo* GetSkillInfo(int skillId, int level, class CCreature *creature = 0);
};

