
#pragma once

#include <Server/CObject.h>
#include <Server/CContributeData.h>
#include <Server/CSkillInfo.h>
#include <Common/CSharedItemData.h>
#include <windows.h>
#include <vector>

class CItem : public CObject {
public:
	class ItemInfo {
	public:
		class Ext {
		public:
			// NO CONSTRUCTOR/DESTRUCTOR - USE ONLY PLAIN OLD DATA! EVERYTHING IS 0 BY DEFAULT!
			bool isPrivateStore;
			bool isPrivateStoreSet;
			bool isOlympiadCanUse;
			bool isOlympiadCanUseSet;
		};

		/* 0x0000 */ unsigned char padding0x0000[0x0008 - 0x0000];
		/* 0x0008 */ UINT32 someType;
		/* 0x000C */ unsigned char padding0x000C[0x0020 - 0x000C];
		/* 0x0020 */ class CSkillInfo *itemSkill;
		/* 0x0028 */ unsigned char padding0x0028[0x0108 - 0x0028];
		/* 0x0108 */ UINT32 dropPeriod;
		/* 0x010C */ unsigned char padding0x010C[0x0144 - 0x010C];
		/* 0x0144 */ int crystalType;
		/* 0x0148 */ Ext ext;
	};

	static void Init();

	class CContributeData* GetContributeData();
	bool IsTradeable(class CUser *user);
	bool IsPrivateSellable(class CUser *user, bool b);
	void SetAttribute(INT16 *attribute);

	static UINT64 __cdecl WarehouseDepositHelper(CItem *item);

	/* 0x0000 */ unsigned char padding0x0000[0x001C - 0x0000];
	/* 0x001C */ UINT32 itemId;
	/* 0x0020 */ unsigned char padding0x0020[0x0048 - 0x0020];
	/* 0x0048 */ CSharedItemData *sd;
	/* 0x0050 */ ItemInfo *itemInfo;
	/* 0x0058 */ class CYieldLock *lock;
	/* 0x0060 */ unsigned char padding0x0060[0x0078 - 0x0060];
	/* 0x0078 */ std::vector<CSkillInfo*> skills;
	/* 0x0098 */ unsigned char padding0x0098[0x00B8 - 0x0098];
	/* 0x00B8 */ UINT32 contributeDataObjectId;
	/* 0x00BC */ unsigned char padding0x00BC[0x0130 - 0x00BC];
	/* 0x0130 */ UINT16 attributeAttackType;
	/* 0x0132 */ UINT16 attributeAttackValue;
	/* 0x0134 */ UINT16 attributeFire;
	/* 0x0136 */ UINT16 attributeWater;
	/* 0x0138 */ UINT16 attributeWind;
	/* 0x013A */ UINT16 attributeEarth;
	/* 0x013C */ UINT16 attributeDivine;
	/* 0x013E */ UINT16 attributeDark;
};

class RefundItem {
private:
	RefundItem();
	RefundItem(const RefundItem &other);
	RefundItem& operator=(const RefundItem &other);

public:
	RefundItem(UINT32 id, CItem *item, INT64 amount, INT64 pricePerUnit);
	~RefundItem();
	bool IsSame(UINT32 itemTypeId, INT64 pricePerUnit);

	CItem *item;
	INT32 id;
	INT64 amount;
	INT64 pricePerUnit;
};

