
#include <Server/CSkillEffect_i_confuse.h>
#include <Server/CCreature.h>
#include <Common/CSharedCreatureData.h>
#include <Common/Utils.h>

void CSkillEffect_i_confuse::Init()
{
	WriteInstructionCall(0x8577BF, FnPtr(IsValidTarget), 0x8577C5);
}

bool CSkillEffect_i_confuse::IsValidTarget(const CCreature *creature)
{
	if (!GetVfn<bool(*)(const CCreature*)>(creature, 40)(creature)) {
		return false;
	}
	if (creature->sd) {
		if (creature->sd->isInsidePeaceZone) {
			return false;
		}
		if (creature->sd->protectAfterLoginExpiry) {
			return false;
		}
	}
	return true;
}

