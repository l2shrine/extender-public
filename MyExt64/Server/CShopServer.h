
#pragma once

#include <Server/CProduct.h>
#include <Common/SmartPtr.h>

class CShopServer {
public:
	static CShopServer* Instance();

	SmartPtr<CProduct> GetProduct(int productId);
};

