
#pragma once

#include <Server/CPremiumServiceSocket.h>
#include <windows.h>

class CPremiumService {
public:
	enum PointType {
		PointDonate = 0,
		PointVote = 1
	};

public:
	static void Init();
	static void Bind();

	static CPremiumService* Instance();
	static bool IsShopPaused();

	static bool ReplyBuyItem(CPremiumServiceSocket* socket, const unsigned char* packet);
	static bool ReplyGetGamePoint(CPremiumServiceSocket* socket, const unsigned char* packet);
	static bool ReplySubGamePoint(CPremiumServiceSocket* socket, const unsigned char* packet);
	static bool ReplyAddGamePoint(CPremiumServiceSocket* socket, const unsigned char* packet);
	static bool ReplyNetPing(CPremiumServiceSocket* socket, const unsigned char* packet);

protected:
	static bool Bind(int id, bool(*function)(CPremiumServiceSocket*, const unsigned char*));
	static int GetOpcodeBuyProduct();
	static int GetOpcodeGamePoint();

public:
	int SendBuyItemRequest(int userId, int productId, int count);
	void RequestGetGamePoint(int userId, int type);
	void RequestSubGamePoint(int userId, INT64 points);
	void RequestAddGamePoint(int userId, INT64 points);

	/* 0x0000 */ unsigned char padding0x0000[0x0010 - 0x0000];
	/* 0x0010 */ bool connected;
	/* 0x0011 */ bool verified;
	/* 0x0012 */ unsigned char padding0x0012[0x0018 - 0x0012];
	/* 0x0018 */ CPremiumServiceSocket *socket;
};

