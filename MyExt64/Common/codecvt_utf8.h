
#pragma once

#include <locale>

class codecvt_utf8 : public std::codecvt<wchar_t, char, std::mbstate_t> {
protected:
	result do_in(std::mbstate_t &state,
		const char *from, const char *fromEnd, const char *&fromNext,
		wchar_t *to, wchar_t *toEnd, wchar_t *&toNext) const;
	result do_out(std::mbstate_t &state,
		const wchar_t *from, const wchar_t *fromEnd, const wchar_t *&fromNext,
		char *to, char *toEnd, char *&toNext) const;
	bool do_always_noconv() const throw();
	int  do_encoding() const throw();
};

