
#pragma once

#include <windows.h>

class CSharedRWLock {
public:
	void ReadLock(bool concession);
	void WriteLock();
	void Done();
};

class CSharedRWLockReadGuard {
private:
	CSharedRWLockReadGuard();
	CSharedRWLockReadGuard(const CSharedRWLockReadGuard &other);
public:
	CSharedRWLockReadGuard(CSharedRWLock *lock);
	~CSharedRWLockReadGuard();
	void Unlock();
	void Lock();
protected:
	CSharedRWLock *lock;
	bool locked;
};

class CSharedRWLockWriteGuard {
private:
	CSharedRWLockWriteGuard();
	CSharedRWLockWriteGuard(const CSharedRWLockWriteGuard &other);
public:
	CSharedRWLockWriteGuard(CSharedRWLock *lock);
	~CSharedRWLockWriteGuard();
	void Unlock();
	void Lock();
protected:
	CSharedRWLock *lock;
	bool locked;
};

