
#pragma once

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/support_multi_pass.hpp>
#include <boost/spirit/include/classic_position_iterator.hpp>

namespace qi = boost::spirit::qi;
namespace wide = qi::standard_wide;

typedef boost::spirit::basic_istream_iterator<wchar_t> ParserStreamIterator;
typedef boost::spirit::multi_pass<ParserStreamIterator> ParserMultiIterator;
typedef boost::spirit::classic::position_iterator<ParserMultiIterator> ParserIterator;

class SkipperParser : public qi::grammar<ParserIterator> {
public:
	SkipperParser();

	qi::rule<ParserIterator> start;
	qi::rule<ParserIterator> whitespace;
	qi::rule<ParserIterator> lineComment;
	qi::rule<ParserIterator> blockComment;
};

class Parser : public qi::grammar<ParserIterator, SkipperParser> {
public:
	typedef ParserIterator Iterator;
	typedef SkipperParser Skipper;
	typedef qi::rule<Iterator> Keyword;
	typedef qi::rule<Iterator, Skipper> Rule;
	typedef qi::rule<Iterator, Skipper, std::wstring()> String;

	Parser();
	bool Parse(const std::wstring &filename, const bool errorIfCantOpen);

	qi::rule<Iterator, wchar_t()> bom;
	qi::rule<Iterator, std::wstring()> identifier;
	qi::rule<Iterator, long()> integer;
	qi::rule<Iterator, double()> decimal;
	qi::rule<Iterator, Skipper> start;
	qi::rule<Iterator> lbracket;
	qi::rule<Iterator> rbracket;
	qi::rule<Iterator> semicolon;
	qi::rule<Iterator> assign;
	qi::rule<Iterator> quote;
	qi::rule<Iterator, std::wstring()> quotedstring;
	qi::rule<Iterator, std::wstring()> bracketstring;
};

