
#include <Common/CSharedRWLock.h>
#include <Common/Utils.h>

void CSharedRWLock::ReadLock(bool concession)
{
	reinterpret_cast<void(*)(CSharedRWLock*, bool)>(0x814284)(this, concession);
}

void CSharedRWLock::WriteLock()
{
	reinterpret_cast<void(*)(CSharedRWLock*)>(0x8143DC)(this);
}

void CSharedRWLock::Done()
{
	reinterpret_cast<void(*)(CSharedRWLock*)>(0x814514)(this);
}

CSharedRWLockReadGuard::CSharedRWLockReadGuard(CSharedRWLock *lock)
	: lock(lock), locked(true)
{
	lock->ReadLock(false);
}

CSharedRWLockReadGuard::~CSharedRWLockReadGuard()
{
	if (locked) {
		lock->Done();
	}
}

void CSharedRWLockReadGuard::Lock()
{
	if (locked) return;
	lock->ReadLock(false);
	locked = true;
}

void CSharedRWLockReadGuard::Unlock()
{
	if (!locked) return;
	lock->Done();
	locked = false;
}

CSharedRWLockWriteGuard::CSharedRWLockWriteGuard(CSharedRWLock *lock)
	: lock(lock), locked(true)
{
	lock->WriteLock();
}

CSharedRWLockWriteGuard::~CSharedRWLockWriteGuard()
{
	if (locked) {
		lock->Done();
	}
}

void CSharedRWLockWriteGuard::Lock()
{
	if (locked) return;
	lock->WriteLock();
	locked = true;
}

void CSharedRWLockWriteGuard::Unlock()
{
	if (!locked) return;
	lock->Done();
	locked = false;
}

