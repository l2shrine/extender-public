
#pragma once

#include <windows.h>

class NPCd {
public:
    static void Init();

protected:
	static void DisableSendMail();
	static void HookStart();
	static void ChangePaths();
	static void InitUtf8Support();
	static void FixWcstol();

public:
	static void Send(const char *format, ...);
	static void SendV(const char *format, va_list va);

protected:
	static HWND __cdecl CreateWindowEx(DWORD dwExStyle, LPCWSTR lpClassName, LPCWSTR lpWindowName, DWORD dwStyle, int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam);
	static void __cdecl StartHook(void *logger, int level, const char *fmt);

public:
	enum PacketEx {
		WHISPER = 1,
		SET_ABILITY_ITEM_DROP = 2,
		SHOW_BUY_SELL = 3,
		IS_TOGGLE_SKILL_ONOFF = 4,
		RIDE_WYVERN2 = 5,
		CLEAR_CONTRIBUTE_DATA = 6
	};
};

