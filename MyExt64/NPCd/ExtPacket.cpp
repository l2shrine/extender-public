
#include <NPCd/ExtPacket.h>
#include <NPCd/CNPC.h>
#include <NPCd/CNPCEvent.h>
#include <NPCd/CNPCEventFactory.h>
#include <NPCd/CCreature.h>
#include <Common/Utils.h>
#include <Common/CLog.h>

void ExtPacket::Init()
{
	WriteMemoryBYTES(0x58BAB3 + 0x4, "\xE7\x04", 2); // NPCProtocol init
	WriteMemoryBYTE(0x58BAC0 + 0x3, 0x9B); // NPCProtocol init

	WriteMemoryBYTES(0x547FDD + 0x3, "\xE8\x04", 2); // Check registered handlers
	WriteMemoryBYTE(0x548026 + 0x2, 0x9B); // Check registered handlers

	WriteMemoryBYTE(0x58250A + 0x3, 0x9A); // Bind
	WriteMemoryBYTES(0x582520 + 0x4, "\xE8\x04", 2); // Bind

	WriteMemoryBYTE(0x58ACC0 + 0x5, 0x9B); // OnRead

	WriteMemoryQWORD(0x63EF78, FnPtr(Register)); // Add registration call to empty place
}

void ExtPacket::Register()
{
	bool result = reinterpret_cast<bool(*)(UINT64, int, UINT32)>(0x5824B8)(
		0x37830E0, 0x9A, FnPtr(Handler));
	*reinterpret_cast<bool*>(0x3783722) = result;
}

bool ExtPacket::Handler(void *socket, unsigned char *packet)
{
	UINT16 opcode = *reinterpret_cast<UINT16*>(packet);
	packet += 2;

	switch (opcode) {
	case IS_TOGGLE_SKILL_ONOFF:
		return IsToggleSkillOnOff(socket, packet);
	default:
		CLog::Add(CLog::Red, L"Unknown ExtPacket opcode 0x%04X", opcode);
		return false;
	}
	return false;
}

bool ExtPacket::IsToggleSkillOnOff(void *socket, const unsigned char *packet)
{
	UINT32 npcId = 0;
	UINT32 targetId = 0;
	UINT32 skillStatus = 0;
	UINT32 skillId = 0;
	Disassemble(packet, "dddd", &npcId, &targetId, &skillStatus, &skillId);
	CNPC *npc = CNPC::GetObject(npcId);
	CCreature *target = CCreature::GetObject(targetId);
	if (!npc || !target || !target->sd) return false;
	CNPCEvent *event = CNPCEventFactory::CreateIsToggleSkillOnOff(npc, target->sd, bool(skillStatus), skillId);
	if (!event) {
		CLog::Add(CLog::Red, L"[%s][%d] Event failed", __FILEW__, __LINE__);
		return false;
	}
	npc->CallEvent(event);
	event->DecRef(__FILE__, __LINE__);
	return false;
}

