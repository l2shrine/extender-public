
#include <NPCd/CNPCEventFactory.h>
#include <Common/Utils.h>

CNPCEvent* CNPCEventFactory::Create()
{
	CNPCEvent *event = reinterpret_cast<CNPCEvent*(*)(const size_t)>(0x47E698)(sizeof(CNPCEvent));
	reinterpret_cast<void(*)(CNPCEvent*, int)>(0x4F8130)(event, 173); // incref
	event->vtablePtr = 0x69E798;
	event->vtablePtr2 = 0x69E778;
	memset(&event->padding0x02D0, 0, sizeof(CNPCEvent) - offsetof(CNPCEvent, padding0x02D0));
	return event;
}

CNPCEvent* CNPCEventFactory::CreateIsToggleSkillOnOff(CNPC *npc, CSharedCreatureData *target, bool onOff, int skillId)
{
	GUARDED;

	CNPCEvent *event = Create();

	if (!event) return 0;

	npc->IncRef(__FILE__, __LINE__);
	event->eventType = CNPCEvent::IS_TOGGLE_SKILL_ONOFF;
	event->myself = npc;
	event->i0 = int(onOff);
	event->i1 = skillId;
	event->target = target;

	return event;
}

