
#include <NPCd/CNPCEvent.h>
#include <Common/Utils.h>

void CNPCEvent::IncRef(const char *file, const int line)
{
	GetVfn<void(*)(CNPCEvent*, const char*, int, int), CNPCEvent>(this, 1)(this, file, line, 1);
}

void CNPCEvent::DecRef(const char *file, const int line)
{
	GetVfn<void(*)(CNPCEvent*, const char*, int, int), CNPCEvent>(this, 2)(this, file, line, 1);
}

CompileTimeOffsetCheck(CNPCEvent, padding0x0318, 0x318);

