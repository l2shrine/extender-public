
#pragma once

#include <Common/CSharedCreatureData.h>

namespace NPC {

class CCreature {
public:
	static CCreature* GetObject(const UINT32 objectId);

	/* 0x0000 */ unsigned char padding0x0000[0x0020 - 0x0008];
	/* 0x0020 */ CSharedCreatureData *sd;
};

} // namespace NPC

using NPC::CCreature;

