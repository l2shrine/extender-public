
#include <NPCd/StaticRespawn.h>
#include <Common/Parser.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

namespace {

class ParserImpl : public Parser {
public:
	ParserImpl()
	{
		static_respawn_begin %= qi::lexeme[wide::string(L"static_respawn_begin")];
		static_respawn_end %= qi::lexeme[wide::string(L"static_respawn_end")];
		name %= qi::lexeme[wide::string(L"name")];
		minimum_delay %= qi::lexeme[wide::string(L"minimum_delay")];
		respawn_time %= qi::lexeme[wide::string(L"respawn_time")];
		respawn_time_random %= qi::lexeme[wide::string(L"respawn_time_random")];
		hour %= qi::lexeme[wide::string(L"hours") | wide::string(L"hour")];
		minute %= qi::lexeme[wide::string(L"minutes") | wide::string(L"minute")];
		second %= qi::lexeme[wide::string(L"seconds") | wide::string(L"second")];
		timeUnit %= hour[boost::lambda::var(currentTimeInterval) *= 3600]
			| minute[boost::lambda::var(currentTimeInterval) *= 60]
			| second;
		nameAssignment %= name > assign > bracketstring[boost::lambda::var(currentName) = boost::lambda::_1];
		timeInterval %= integer[boost::lambda::var(currentTimeInterval) = boost::lambda::_1] > timeUnit;
		minimumDelayAssignment %= minimum_delay > assign > timeInterval[boost::lambda::var(currentDef.minimumDelaySeconds) = boost::lambda::var(currentTimeInterval)];
		respawnTimeAssignment %= respawn_time > assign
			> integer[boost::lambda::var(currentDef.respawnTimeHour) = boost::lambda::_1]
			> qi::lexeme[wide::string(L":")]
			> integer[boost::lambda::var(currentDef.respawnTimeMinute) = boost::lambda::_1];
		respawnTimeRandomAssignment %= respawn_time_random > assign > timeInterval[boost::lambda::var(currentDef.respawnTimeRandomSeconds) = boost::lambda::var(currentTimeInterval)];
		definition %= static_respawn_begin[boost::lambda::var(currentDef) = StaticRespawn::StaticRespawnDef()]
			> (nameAssignment ^ minimumDelayAssignment ^ respawnTimeAssignment ^ respawnTimeRandomAssignment)
			> static_respawn_end[
				boost::lambda::bind(&std::map<std::wstring, StaticRespawn::StaticRespawnDef>::operator[], &StaticRespawn::defs, boost::lambda::var(currentName)) = boost::lambda::var(currentDef)];
		start %= -bom > *definition > qi::eoi;
	}

	Keyword static_respawn_begin, static_respawn_end, name, minimum_delay, respawn_time, respawn_time_random, hour, minute, second;
	Rule definition, nameAssignment, minimumDelayAssignment, respawnTimeAssignment, respawnTimeRandomAssignment, timeUnit, timeInterval;
	StaticRespawn::StaticRespawnDef currentDef;
	std::wstring currentName;
	long currentTimeInterval;
};

} // namespace

std::map<std::wstring, StaticRespawn::StaticRespawnDef> StaticRespawn::defs;

StaticRespawn::StaticRespawnDef::StaticRespawnDef() : minimumDelaySeconds(0), respawnTimeHour(0), respawnTimeMinute(0), respawnTimeRandomSeconds(0)
{
}

void StaticRespawn::Load()
{
	CLog::Add(CLog::Blue, L"Reading ..\\Script\\staticrespawn.txt");
	if (!ParserImpl().Parse(L"..\\Script\\staticrespawn.txt", false)) {
		CLog::Add(CLog::Red, L"Failed to load staticrespawn.txt");
	} else {
		CLog::Add(CLog::Blue, L"Loaded ..\\Script\\staticrespawn.txt");
	}
}

