
#include <NPCd/Functions/CNPC_IsToggleSkillOnOff.h>
#include <NPCd/NPCd.h>
#include <Common/CLog.h>

CNPC_IsToggleSkillOnOff::CNPC_IsToggleSkillOnOff() :
	NPCFunction(L"IsToggleSkillOnOff", &IsToggleSkillOnOff)
{
}

void* CNPC_IsToggleSkillOnOff::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*, void*)>(functionPtr.functionPtr)(
		caller, params[0], params[1]);
}

void CNPC_IsToggleSkillOnOff::SetTypes()
{
	SetReturnType(Type::TYPE_VOID);
	AddParameter(Type::TYPE_CREATURE);
	AddParameter(Type::TYPE_INT);
}

int CNPC_IsToggleSkillOnOff::IsToggleSkillOnOff(CNPC *npc, CSharedCreatureData *target, int skillUid)
{
	NPCd::Send("chddd", 0x36, NPCd::IS_TOGGLE_SKILL_ONOFF, npc->sm->index, target->index, skillUid);
	return 0;
}

