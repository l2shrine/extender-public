
#pragma once

namespace Cached {

class ExtPacket {
public:
	static void Init();
	static void Register();
	static bool Handler(class CSocket *socket, const unsigned char *packet);
	static bool ExtHandler(class CSocket *socket, const unsigned char *packet);
	static bool TestPacket(class CSocket *socket, const unsigned char *packet);
	static bool SendPostPacket(class CSocket *socket, const unsigned char *packet);
	static bool ReceivePostListPacket(class CSocket *socket, const unsigned char *packet);
	static bool SentPostListPacket(class CSocket *socket, const unsigned char *packet);
	static bool ReceivedPostPacket(class CSocket *socket, const unsigned char *packet);
	static bool SentPostPacket(class CSocket *socket, const unsigned char *packet);
	static bool DeleteReceivedPostPacket(class CSocket *socket, const unsigned char *packet);
	static bool DeleteSentPostPacket(class CSocket *socket, const unsigned char *packet);
	static bool ReceivePostPacket(class CSocket *socket, const unsigned char *packet);
	static bool CheckMailPacket(class CSocket *socket, const unsigned char *packet);
	static bool RejectPostPacket(class CSocket *socket, const unsigned char *packet);
	static bool CancelPostPacket(class CSocket *socket, const unsigned char *packet);
	static bool MailUpkeepPacket(class CSocket *socket, const unsigned char *packet);
};

} // namespace Cached

