
#pragma once

#include <Common/SmartPtr.h>

namespace Cached {

class CWareHouse;

template<class T>
class CObjectStorage {
public:
	CObjectStorage() : object(0)
	{
	}

	~CObjectStorage()
	{
		if (object) object->Release(__FILEW__, __LINE__, 8);
	}

	void SetObject(T *object);

	SmartPtr<T> GetObject();

	T *object;
};

template<>
void CObjectStorage<CWareHouse>::SetObject(CWareHouse *object);

template<>
SmartPtr<CWareHouse> CObjectStorage<CWareHouse>::GetObject();

} // namespace Cached

