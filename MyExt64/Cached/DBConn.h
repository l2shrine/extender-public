
#pragma once

#include <Windows.h>
#include <sqltypes.h>

class DBConn {
public:
	DBConn();
	~DBConn();

	static void Test();

	void Bind(int *i);
	void Bind(UINT32 *i);
	void Bind(INT64 *i);
	void Bind(wchar_t *buffer, const size_t sizeInBytes);

	void Unbind();

	void Prepare(const wchar_t *query);

	void BindParameter(const UINT32 &value);
	void BindParameter(const INT32 &value);
	void BindParameter(const UINT64 &value);
	void BindParameter(const INT64 &value);

	template<class T>
	void BindParameter(const T *value);

	template<>
	void BindParameter(const wchar_t *value)
	{
		reinterpret_cast<void(*)(DBConn*, const wchar_t*)>(0x40121C)(this, value);
	}

	bool Execute();
	bool Fetch();

	/* 0x0000 */ SQLHSTMT handle;
	/* 0x0008 */ unsigned char padding0x0008[0x001A - 0x0008];
	/* 0x001A */ UINT16 parameterNo;
	/* 0x001C */ unsigned char padding0x001C[0x0420 - 0x001C];
	/* 0x0420 */ // ext begin
	const wchar_t *preparedQuery;
};

