
#include <Cached/CUser.h>
#include <Cached/CWareHouse.h>
#include <Common/Utils.h>
#include <Common/CLog.h>

namespace Cached {

std::map<UINT32, std::set<UINT32> > CUser::alreadyRefundedItems;
CriticalSection CUser::alreadyRefundedItemsCS;

void CUser::Init()
{
	WriteMemoryDWORD(0x55090B + 0x1, sizeof(CUser));
	WriteInstructionCall(0x550D35, FnPtr(Constructor));
	WriteMemoryQWORD(0x658058, FnPtr(Destructor));
	WriteInstructionCall(0x4E2C5B, FnPtr(&CUser::WriteLogin));
	WriteInstructionCall(0x475BAD, FnPtr(&CUser::WriteLogout));
	WriteInstructionCall(0x4E317E, FnPtr(&CUser::WriteLogout));
	WriteInstructionCall(0x4E2871, FnPtr(&CUser::BuyItemsPacketGetWarehouse));
	WriteInstructionCall(0x47AC84, FnPtr(&CUser::GetWareHouse));
	WriteInstructionCall(0x47B411, FnPtr(&CUser::GetWareHouse));
}

CUser* CUser::Constructor(CUser *self, wchar_t *a, wchar_t *b, unsigned int c, unsigned int d, unsigned int e,
	int f, int g, int h, int i, int j, int k, int l, int m, double n, double o, double p, double q, unsigned int r,
	INT64 s, int t, int u, unsigned int v, unsigned int w, unsigned int x, struct AbnormalStatus *y, int z,
	int aa, int ab, int ac, wchar_t *ad, unsigned char *ae, unsigned int af, unsigned int ag, unsigned int ah,
	unsigned int ai, unsigned int aj, int ak, int al, int am, int an, int ao, int ap, int aq, int ar, int as,
	int at, int au, int av, double aw, double ax, int ay, int az, int ba, int bb, int bc, int bd, int be,
	int bf, int bg, int bh, int bi, int bj, int bk, int bl, int bm, int bn, int bo, int bp, int bq, int br,
	int bs, int bt, int bu, int bv, int bw, int bx, int by, int bz, int ca, int cb, int cc)
{
	GUARDED;

	CUser *ret = reinterpret_cast<CUser*(*)(CUser*, wchar_t*, wchar_t*, unsigned int, unsigned int, unsigned int,
		int, int, int, int, int, int, int, int, double, double, double, double, unsigned int,
		INT64, int, int, unsigned int, unsigned int, unsigned int, struct AbnormalStatus*, int,
		int, int, int, wchar_t*, unsigned char*, unsigned int, unsigned int, unsigned int,
		unsigned int, unsigned int, int, int, int, int, int, int, int, int, int,
		int, int, int, double, double, int, int, int, int, int, int, int,
		int, int, int, int, int, int, int, int, int, int, int, int, int,
		int, int, int, int, int, int, int, int, int, int, int)>(0x54FAA0)(
			self, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z,
			aa, ab, ac, ad, ae, af, ag, ah, ai, aj, ak, al, am, an, ao, ap, aq, ar, as, at, au, av, aw, ax, ay, az,
			ba, bb, bc, bd, be, bf, bg, bh, bi, bj, bk, bl, bm, bn, bo, bp, bq, br, bs, bt, bu, bv, bw, bx, by, bz,
			ca, cb, cc);

	new (&ret->ext) Ext();

	return ret;
}

CUser* CUser::Destructor(CUser *self, bool isMemoryFreeUsed)
{
	GUARDED;
	self->ext.~Ext();
	return reinterpret_cast<CUser*(*)(CUser*, bool)>(0x550874)(self, isMemoryFreeUsed);
}

CUser::Ext::Ext()
{
	CWareHouse *tmp = reinterpret_cast<CWareHouse*(*)(const size_t)>(0x44C5F8)(sizeof(CWareHouse));
	tmp->ownerDBID = GetUser()->dbId;
	tmp->Initialize(CWareHouse::MAIL, true);
	mailWarehouse.SetObject(tmp);
	tmp->Release(__FILEW__, __LINE__, 1);
}

CUser::Ext::~Ext()
{
}

UINT32 CUser::GetId()
{
	return reinterpret_cast<UINT32(*)(CUser*)>(0x52DE00)(this);
}

void CUser::WriteLogin(INT32 i)
{
	reinterpret_cast<void(*)(CUser*, INT32)>(0x5396EC)(this, i);
	{
		ScopedLock lock(alreadyRefundedItemsCS);
		alreadyRefundedItems[GetId()].clear();
	}
}

void CUser::WriteLogout(INT32 i, INT32 j)
{
	{
		ScopedLock lock(alreadyRefundedItemsCS);
		std::map<UINT32, std::set<UINT32> >::iterator i = alreadyRefundedItems.find(GetId());
		if (i != alreadyRefundedItems.end()) {
			alreadyRefundedItems.erase(i);
		}
	}
	reinterpret_cast<void(*)(CUser*, INT32, INT32)>(0x539A48)(this, i, j);
}

void* CUser::BuyItemsPacketGetWarehouse(void *smartptr, int i, bool b)
{
	// store user for later use
	CWareHouse::buySellExt[GetThreadIndex()].userId = GetId();
	return reinterpret_cast<void*(*)(CUser*, void*, int, bool)>(0x543524)(this, smartptr, i, b);
}

SmartPtr<CWareHouse> CUser::GetWareHouse(const int type, const bool load)
{
	SmartPtr<CWareHouse> ret;
	switch (type) {
	case CWareHouse::MAIL:
		ret = ext.mailWarehouse.GetObject();
		if (!ret) {
			CLog::Add(CLog::Red, L"Can't get object from mail warehouse storage");
			return ret;
		}
		if (load && !ret->loaded) {
			ret->LoadItems(type);
		}
		break;
	default:
		reinterpret_cast<void(*)(CUser*, SmartPtr<CWareHouse>*, int, bool)>(0x543524)(
			this, &ret, type, load);
		break;
	}
	return ret;
}

SmartPtr<CUser> CUser::GetUser(const int dbId, const bool load, const bool mustExist)
{
	SmartPtr<CUser> ret;
	reinterpret_cast<void(*)(UINT64, SmartPtr<CUser>*, int, bool, bool)>(0x5512B8)(
		0x273A1D0, &ret, dbId, load, mustExist);
	return ret;
}

int CUser::GetAccountID()
{
	return reinterpret_cast<int(*)(CUser*)>(0x52EA84)(this);
}

int CUser::GetX()
{
	return reinterpret_cast<int(*)(CUser*)>(0x52ED40)(this);
}

int CUser::GetY()
{
	return reinterpret_cast<int(*)(CUser*)>(0x52EDA4)(this);
}

int CUser::GetZ()
{
	return reinterpret_cast<int(*)(CUser*)>(0x52EE08)(this);
}

int CUser::GetRace()
{
	return reinterpret_cast<int(*)(CUser*)>(0x52EC14)(this);
}

int CUser::GetGender()
{
	return reinterpret_cast<int(*)(CUser*)>(0x52EBB0)(this);
}

int CUser::GetClass()
{
	return reinterpret_cast<int(*)(CUser*)>(0x52EC78)(this);
}

int CUser::GetLevel()
{
	return reinterpret_cast<int(*)(CUser*)>(0x52F0EC)(this);
}

const wchar_t* CUser::GetCharName()
{
	return reinterpret_cast<const wchar_t*(*)(CUser*)>(0x52E9BC)(this);
}

const wchar_t* CUser::GetAccountName()
{
	return reinterpret_cast<const wchar_t*(*)(CUser*)>(0x52EA20)(this);
}

CompileTimeOffsetCheck(CUser, ext, 0x9B0);

} // namespace Cached

