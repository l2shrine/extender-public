
#include <Cached/MailSystem.h>
#include <Cached/DBConn.h>
#include <Cached/CSocket.h>
#include <Cached/CUser.h>
#include <Cached/CWareHouse.h>
#include <Cached/CItem.h>
#include <Common/CLog.h>
#include <Common/Utils.h>
#include <Common/Config.h>
#include <Server/CDB.h>
#include <vector>
#include <list>

namespace Cached {

MailSystem::PostLocker MailSystem::locker;

void MailSystem::PostLocker::Lock(const int id)
{
	cs[id % (sizeof(cs) / sizeof(cs[0]))].Lock();
}

void MailSystem::PostLocker::Unlock(const int id)
{
	cs[id % (sizeof(cs) / sizeof(cs[0]))].UnLock();
}

MailSystem::PostLocker::Guard::Guard(PostLocker &locker, const int id) : locker(locker), id(id)
{
	locker.Lock(id);
}

MailSystem::PostLocker::Guard::~Guard()
{
	locker.Unlock(id);
}

MailSystem::Attachment MailSystem::Attachment::FromPacket(const unsigned char **packet)
{
	Attachment ret;
	*packet = Disassemble(*packet, "ddQ", &ret.itemDbId, &ret.itemObjectId, &ret.amount);
	return ret;
}

MailSystem::Post MailSystem::Post::FromPacket(const unsigned char **packet)
{
	Post ret;
	wchar_t recipient[25];
	wchar_t subject[128];
	wchar_t content[512];
	int attachmentCount = 0;
	*packet = Disassemble(*packet, "ddSSSQd", &ret.senderDbId, &ret.senderObjectId,
		sizeof(recipient), recipient,
		sizeof(subject), subject,
		sizeof(content), content,
		&ret.codAdena, &attachmentCount);
	ret.recipient = recipient;
	ret.subject = subject;
	ret.content = content;
	for (size_t i = 0 ; i < attachmentCount ; ++i) {
		ret.attachments.push_back(Attachment::FromPacket(packet));
	}
	return ret;
}

void MailSystem::SendPost(CSocket *socket, Post &post)
{
	GUARDED;

	int recipientDbId = 0;
	wchar_t recipientName[25];

	{
		DBConn conn;
		conn.Prepare(L"SELECT ud.char_id, ud.char_name FROM dbo.user_data ud WHERE ud.char_name=?");
		conn.Bind(&recipientDbId);
		conn.Bind(recipientName, sizeof(recipientName));
		conn.BindParameter(post.recipient.c_str());
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't get recipient ID for character %s from DB", post.recipient.c_str());
			return;
		}
		if (!conn.Fetch()) {
			socket->Send("chhd", 0xD2, 0x0077, CDB::REPLY_SEND_POST_RECIPIENT_NOT_FOUND,
				post.senderObjectId);
			return;
		}
		if (recipientDbId == post.senderDbId) {
			socket->Send("chhd", 0xD2, 0x0077, CDB::REPLY_SEND_POST_RECIPIENT_IS_SENDER,
				post.senderObjectId);
			return;
		}
		conn.Unbind();
		conn.Prepare(L"SELECT COUNT(*) FROM dbo.user_blocklist WHERE char_id=? AND block_char_id=?");
		int count = 0;
		conn.Bind(&count);
		conn.BindParameter(recipientDbId);
		conn.BindParameter(post.senderDbId);
		if (!conn.Execute() || !conn.Fetch()) {
			CLog::Add(CLog::Red, L"DB error [%s][%d]", __FILEW__, __LINE__);
			return;
		}
		if (count) {
			socket->Send("chhd", 0xD2, 0x0077, CDB::REPLY_SEND_POST_RECIPIENT_BLOCKED_SENDER,
				post.senderObjectId);
			return;
		}
	}

	post.recipient = recipientName;

	int currentPostCount = 0;
	{
		DBConn conn;
		conn.Prepare(L"SELECT COUNT(*) FROM dbo.post WHERE recipient_id=? AND recipient_deleted=0");
		conn.Bind(&currentPostCount);
		conn.BindParameter(recipientDbId);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't get recipient's %d mail count from DB", recipientDbId);
			return;
		}
		if (!conn.Fetch()) {
			CLog::Add(CLog::Red, L"Can't fetch recipient's %d mail count from DB", recipientDbId);
			return;
		}
		if (currentPostCount >= 240) {
			socket->Send("chhd", 0xD2, 0x0077, CDB::REPLY_SEND_POST_RECIPIENT_MAILBOX_FULL,
				post.senderObjectId);
			return;
		}
	}

	{
		wchar_t sender[25];
		DBConn conn;
		conn.Prepare(L"SELECT ud.char_name FROM dbo.user_data ud WHERE ud.char_id=?");
		conn.Bind(sender, sizeof(sender));
		conn.BindParameter(post.senderDbId);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't get sender %d name from DB", post.senderDbId);
			return;
		}
		if (!conn.Fetch()) {
			CLog::Add(CLog::Red, L"Can't fetch sender %d name from DB", post.senderDbId);
			return;
		}
		post.sender = sender;
	}

	SmartPtr<CUser> senderUser = CUser::GetUser(post.senderDbId, true, false);
	if (!senderUser) {
		CLog::Add(CLog::Red, L"Can't get sender user");
		return;
	}

	SmartPtr<CUser> recipientUser = CUser::GetUser(recipientDbId, true, false);
	if (!recipientUser) {
		CLog::Add(CLog::Red, L"Can't get recipient user");
		return;
	}

	SmartPtr<CWareHouse> inventory = senderUser->GetWareHouse(CWareHouse::INVENTORY, true);
	if (!inventory) {
		CLog::Add(CLog::Red, L"Can't get inventory warehouse");
		return;
	}

	SmartPtr<CWareHouse> mail = senderUser->GetWareHouse(CWareHouse::MAIL, true);
	if (!mail) {
		CLog::Add(CLog::Red, L"Can't get mail warehouse");
		return;
	}

	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t itemsWritten = 0;

	{
		CWareHouse::LockGuard2 lock(&*inventory, &*mail);
		lock.Lock(__FILEW__, __LINE__);

		CWareHouse::TransactionGuard transactionGuard;

		if (mail->itemCount + post.attachments.size() > 200) {
			CLog::Add(CLog::Red, L"Not enough room in user [%s] mail warehouse", post.sender.c_str());
			return;
		}

		INT64 totalCost = Config::Instance()->mail->mailCost;
		totalCost += post.attachments.size() * Config::Instance()->mail->mailAttachmentCost;

		SmartPtr<CItem> adenaItem = inventory->GetAdena();
		if (!adenaItem || adenaItem->Amount() < totalCost) {
			socket->Send("chhd", 0xD2, 0x0077, CDB::REPLY_SEND_POST_NOT_ENOUGH_ADENA,
				post.senderObjectId);
			return;
		}

		bool adenaBeingSent = false;

		std::set<int> uniqueIds;
		for (std::vector<Attachment>::const_iterator i = post.attachments.begin() ; i != post.attachments.end() ; ++i) {
			if (i->amount < 1 || i->amount >= 0x1000000000000000L) {
				CLog::Add(CLog::Red, L"Invalid amount for item %d (amount = %ld)",
					i->itemDbId, i->amount);
				return;
			}
			if (!uniqueIds.insert(i->itemDbId).second) {
				CLog::Add(CLog::Red, L"Duplicate item %d", i->itemDbId);
				return;
			}
			if (!adenaBeingSent && i->itemDbId == adenaItem->GetDBID()) {
				if (adenaItem->Amount() - totalCost < i->amount) {
					socket->Send("chhd", 0xD2, 0x0077, CDB::REPLY_SEND_POST_NOT_ENOUGH_ADENA,
						post.senderObjectId);
					return;
				}
				adenaBeingSent = true;
			}
		}

		int attachments[8];
		memset(attachments, 0, sizeof(attachments));

		size_t index = 0;
		for (std::vector<Attachment>::const_iterator i = post.attachments.begin() ; i != post.attachments.end() ; ++i, ++index) {

			CWareHouse::TransactionGuard::Item &oldItem(
				transactionGuard.Update(
					inventory->GetItem(i->itemDbId)));

			if (!oldItem) {
				CLog::Add(CLog::Red, L"Can't get item %d", i->itemDbId);
				return;
			}

			if (oldItem->amount < i->amount || (!oldItem->IsStackable() && i->amount != 1)) {
				CLog::Add(CLog::Red, L"Invalid amount for item %d (amount = %ld, requested = %ld)",
					i->itemDbId, oldItem->amount, i->amount);
				return;
			}

			INT64 remaining = 0;
			if (oldItem->amount == i->amount) {
				oldItem->SetWarehouse(mail->GetWarehouseNo());
				oldItem->SetInventorySlotIndex(-1);
				attachments[index] = oldItem->GetDBID();
				oldItem.AddAudit(2001, &*senderUser, &*recipientUser, &*oldItem, -i->amount, 0);
				oldItem.AddAudit(2002, &*recipientUser, &*senderUser, &*oldItem, i->amount, i->amount);
			} else {
				oldItem->SetAmount(oldItem->amount - i->amount);
				remaining = oldItem->Amount();
				CWareHouse::TransactionGuard::Item &newItem(
					transactionGuard.Create(
						mail->MakeNewItem(post.senderDbId, oldItem->ItemType(), i->amount, mail->GetWarehouseNo())));

				if (!newItem) {
					CLog::Add(CLog::Red, L"Failed to create new item owner = %d, type = %d, amount = %ld",
						post.senderDbId, oldItem->ItemType(), i->amount);
					return;
				}
				attachments[index] = newItem->GetDBID();
				oldItem.AddAudit(2001, &*senderUser, &*recipientUser, &*oldItem, -i->amount, oldItem->Amount());
				newItem.AddAudit(2002, &*recipientUser, &*senderUser, &*newItem, i->amount, newItem->Amount());
			}
			if (oldItem->GetDBID() != adenaItem->GetDBID()) {
				bytesWritten += Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "dQ",
					oldItem->GetDBID(), remaining);
				++itemsWritten;
			} else {
				oldItem.AddAudit(2000, &*senderUser, 0, &*oldItem, -totalCost, oldItem->Amount() - totalCost);
			}
		}

		if (!adenaBeingSent) {
			CWareHouse::TransactionGuard::Item &item(transactionGuard.Update(adenaItem));
			item.AddAudit(2000, &*senderUser, 0, &*item, -totalCost, item->Amount() - totalCost);
		}
		adenaItem->SetAmount(adenaItem->Amount() - totalCost);
		bytesWritten += Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "dQ",
			adenaItem->GetDBID(), adenaItem->Amount());
		++itemsWritten;
		if (!adenaItem->Amount()) {
			adenaItem->TransactDelete();
		}

		int expireSeconds = (post.codAdena > 0)
			? Config::Instance()->mail->expireSecondsCod
			: Config::Instance()->mail->expireSeconds;

		{
			DBConn conn;
			conn.Prepare(L"INSERT INTO dbo.post "
				L"(sender_id, sender_name, recipient_id, recipient_name, subject, content, cod_adena, "
				L"item1, item2, item3, item4, item5, item6, item7, item8, create_date, expire_date)"
				L" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, GETUTCDATE(), DATEADD(SECOND, ?, GETUTCDATE()))");
			conn.BindParameter(post.senderDbId);
			conn.BindParameter(post.sender.c_str());
			conn.BindParameter(recipientDbId);
			conn.BindParameter(post.recipient.c_str());
			conn.BindParameter(post.subject.c_str());
			conn.BindParameter(post.content.c_str());
			conn.BindParameter(post.codAdena);
			for (size_t i = 0 ; i < 8 ; ++i) conn.BindParameter(attachments[i]);
			conn.BindParameter(expireSeconds);
			if (!conn.Execute()) {
				CLog::Add(CLog::Red, L"Can't insert post into DB");
				return;
			}
		}

		transactionGuard.Commit();
	}

	socket->Send("chhdddb", 0xD2, 0x0077, CDB::REPLY_SEND_POST_DONE,
		post.senderObjectId, recipientDbId, itemsWritten, bytesWritten, buffer);
}

void MailSystem::ReceivePostList(class CSocket *socket, const int userDbId, const int userObjectId)
{
	GUARDED;

	int id = 0;
	INT64 createDate = 0;
	INT64 expireDate = 0;
	wchar_t senderName[25];
	wchar_t subject[128];
	int read = 0;
	INT64 codAdena = 0;
	int items[8];
	DBConn conn;
	conn.Prepare(
		L"SELECT TOP 240 p.id, DATEDIFF(SECOND, {d'1970-01-01'}, p.create_date), "
			L"DATEDIFF(SECOND, {d'1970-01-01'}, p.expire_date), "
			L"p.sender_name, p.subject, p.[read], p.cod_adena, "
			L"p.item1, p.item2, p.item3, p.item4, "
			L"p.item5, p.item6, p.item7, p.item8 "
		L"FROM dbo.post p "
		L"WHERE p.recipient_id=? "
		L"AND p.recipient_deleted=0 "
		L"ORDER BY p.create_date DESC");
	conn.Bind(&id);
	conn.Bind(&createDate);
	conn.Bind(&expireDate);
	conn.Bind(senderName, sizeof(senderName));
	conn.Bind(subject, sizeof(subject));
	conn.Bind(&read);
	conn.Bind(&codAdena);
	for (size_t i = 0 ; i < 8 ; ++i) conn.Bind(&items[i]);
	conn.BindParameter(userDbId);
	if (!conn.Execute()) {
		CLog::Add(CLog::Red, L"Can't get post list from DB");
		return;
	}
	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t postCount = 0;
	memset(items, 0, sizeof(items));
	while (conn.Fetch()) {
		bool attachments = false;
		for (size_t i = 0 ; i < 8 ; ++i) {
			if (!items[i]) continue;
			attachments = true;
		}
		size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten,
			"dQQSSddd", id, createDate, expireDate, senderName, subject, read, codAdena > 0, int(attachments));
		if (!n) {
			CLog::Add(CLog::Red, L"Overflow in assemble received post list");
			break;
		}
		bytesWritten += n;
		++postCount;
		memset(items, 0, sizeof(items));
	}
	socket->Send("chhddb", 0xD2, 0x0077, CDB::REPLY_RECEIVE_POST_LIST,
		userObjectId, postCount, bytesWritten, buffer);

}

void MailSystem::SentPostList(class CSocket *socket, const int userDbId, const int userObjectId)
{
	GUARDED;

	int id = 0;
	INT64 createDate = 0;
	INT64 expireDate = 0;
	wchar_t recipientName[25];
	wchar_t subject[128];
	int read = 0;
	INT64 codAdena = 0;
	int items[8];
	DBConn conn;
	conn.Prepare(
		L"SELECT TOP 240 p.id, DATEDIFF(SECOND, {d'1970-01-01'}, p.create_date), "
		L"DATEDIFF(SECOND, {d'1970-01-01'}, p.expire_date), "
		L"p.recipient_name, p.subject, p.[read], p.cod_adena, "
		L"p.item1, p.item2, p.item3, p.item4, "
		L"p.item5, p.item6, p.item7, p.item8 "
		L"FROM dbo.post p "
		L"WHERE p.sender_id=? "
		L"AND p.sender_deleted=0 "
		L"ORDER BY p.create_date DESC");
	conn.Bind(&id);
	conn.Bind(&createDate);
	conn.Bind(&expireDate);
	conn.Bind(recipientName, sizeof(recipientName));
	conn.Bind(subject, sizeof(subject));
	conn.Bind(&read);
	conn.Bind(&codAdena);
	for (size_t i = 0 ; i < 8 ; ++i) conn.Bind(&items[i]);
	conn.BindParameter(userDbId);
	if (!conn.Execute()) {
		CLog::Add(CLog::Red, L"Can't get post list from DB");
		return;
	}
	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t postCount = 0;
	memset(items, 0, sizeof(items));
	while (conn.Fetch()) {
		bool attachments = false;
		for (size_t i = 0 ; i < 8 ; ++i) {
			if (!items[i]) continue;
			attachments = true;
		}
		size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten,
			"dQQSSddd", id, createDate, expireDate, recipientName, subject, read, codAdena > 0, int(attachments));
		if (!n) {
			CLog::Add(CLog::Red, L"Overflow in assemble sent post list");
			break;
		}
		bytesWritten += n;
		++postCount;
		memset(items, 0, sizeof(items));
	}
	socket->Send("chhddb", 0xD2, 0x0077, CDB::REPLY_SENT_POST_LIST,
		userObjectId, postCount, bytesWritten, buffer);
}

void MailSystem::ReceivedPost(class CSocket *socket, const int userDbId, const int userObjectId, const int id)
{
	GUARDED;

	int senderDbId = 0;
	wchar_t sender[25];
	wchar_t subject[128];
	wchar_t content[512];
	INT64 adena = 0;
	int items[8];
	memset(items, 0, sizeof(items));
	{
		DBConn conn;
		conn.Prepare(
			L"SELECT p.sender_id, p.sender_name, p.subject, p.content, p.cod_adena, "
			L"p.item1, p.item2, p.item3, p.item4, "
			L"p.item5, p.item6, p.item7, p.item8 "
			L"FROM dbo.post p "
			L"WHERE p.recipient_id=? "
			L"AND p.id=? "
			L"AND p.recipient_deleted=0");
		conn.Bind(&senderDbId);
		conn.Bind(sender, sizeof(sender));
		conn.Bind(subject, sizeof(subject));
		conn.Bind(content, sizeof(content));
		conn.Bind(&adena);
		for (size_t i = 0 ; i < 8 ; ++i) conn.Bind(&items[i]);
		conn.BindParameter(userDbId);
		conn.BindParameter(id);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't get post from DB");
			return;
		}
		if (!conn.Fetch()) {
			return;
		}
	}

	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t attachmentsWritten = 0;

	bool systemMail = false;
	if (!senderDbId) {
		senderDbId = userDbId;
		systemMail = true;
	}

	SmartPtr<CUser> senderUser = CUser::GetUser(senderDbId, true, false);
	if (!senderUser) {
		CLog::Add(CLog::Red, L"Can't get user %d", senderDbId);
		return;
	}

	SmartPtr<CWareHouse> senderWarehouse = senderUser->GetWareHouse(CWareHouse::MAIL, true);
	if (!senderWarehouse) {
		CLog::Add(CLog::Red, L"Can't get user %d mail warehouse", senderDbId);
		return;
	}

	{
		CWareHouse::LockGuard lock(&*senderWarehouse, false);
		lock.Lock(__FILEW__, __LINE__);

		for (size_t i = 0 ; i < 8 ; ++i) {
			if (!items[i]) continue;
			SmartPtr<CItem> item = senderWarehouse->GetItem(items[i]);
			if (!item) {
				CLog::Add(CLog::Red, L"Can't get user %d mail warehouse item %d", senderDbId, items[i]);
				return;
			}
			size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "ddQdhhhhhhhh",
				item->ItemType(), items[i], item->Amount(), item->enchant,
				item->attribute[0], item->attribute[1], item->attribute[2], item->attribute[3],
				item->attribute[4], item->attribute[5], item->attribute[6], item->attribute[7]);
			if (!n) {
				CLog::Add(CLog::Red, L"Overflow in assemble received post attachments");
				break;
			}
			bytesWritten += n;
			++attachmentsWritten;
		}
	}

	{
		DBConn conn;
		conn.Prepare(L"UPDATE dbo.post SET [read]=1 WHERE id=?");
		conn.BindParameter(id);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't update post read");
			return;
		}
	}

	socket->Send("chhddSSSQddb", 0xD2, 0x0077, CDB::REPLY_RECEIVED_POST,
		userObjectId, id, sender, subject, content, adena, int(systemMail), attachmentsWritten, bytesWritten, buffer);
}

void MailSystem::SentPost(class CSocket *socket, const int userDbId, const int userObjectId, const int id)
{
	GUARDED;

	wchar_t recipient[25];
	wchar_t subject[128];
	wchar_t content[512];
	int items[8];
	INT64 adena = 0;
	memset(items, 0, sizeof(items));
	DBConn conn;
	conn.Prepare(
		L"SELECT p.recipient_name, p.subject, p.content, p.cod_adena, "
		L"p.item1, p.item2, p.item3, p.item4, "
		L"p.item5, p.item6, p.item7, p.item8 "
		L"FROM dbo.post p "
		L"WHERE p.sender_id=? "
		L"AND p.id=? "
		L"AND p.sender_deleted=0");
	conn.Bind(recipient, sizeof(recipient));
	conn.Bind(subject, sizeof(subject));
	conn.Bind(content, sizeof(content));
	conn.Bind(&adena);
	for (size_t i = 0 ; i < 8 ; ++i) conn.Bind(&items[i]);
	conn.BindParameter(userDbId);
	conn.BindParameter(id);
	if (!conn.Execute()) {
		CLog::Add(CLog::Red, L"Can't get post from DB");
		return;
	}
	if (!conn.Fetch()) {
		return;
	}

	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t attachmentsWritten = 0;

	SmartPtr<CUser> senderUser = CUser::GetUser(userDbId, true, false);
	if (!senderUser) {
		CLog::Add(CLog::Red, L"Can't get user %d", userDbId);
		return;
	}

	SmartPtr<CWareHouse> senderWarehouse = senderUser->GetWareHouse(CWareHouse::MAIL, true);
	if (!senderWarehouse) {
		CLog::Add(CLog::Red, L"Can't get user %d mail warehouse", userDbId);
		return;
	}

	{
		CWareHouse::LockGuard lock(&*senderWarehouse, false);
		lock.Lock(__FILEW__, __LINE__);

		for (size_t i = 0 ; i < 8 ; ++i) {
			if (!items[i]) continue;
			SmartPtr<CItem> item = senderWarehouse->GetItem(items[i]);
			if (!item) {
				CLog::Add(CLog::Red, L"Can't get user %d mail warehouse item %d", userDbId, items[i]);
				return;
			}
			size_t n = Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "ddQdhhhhhhhh",
				item->ItemType(), items[i], item->Amount(), item->enchant,
				item->attribute[0], item->attribute[1], item->attribute[2], item->attribute[3],
				item->attribute[4], item->attribute[5], item->attribute[6], item->attribute[7]);
			if (!n) {
				CLog::Add(CLog::Red, L"Overflow in assemble received post attachments");
				break;
			}
			bytesWritten += n;
			++attachmentsWritten;
		}
	}

	socket->Send("chhddSSSQdb", 0xD2, 0x0077, CDB::REPLY_SENT_POST,
		userObjectId, id, recipient, subject, content, adena, attachmentsWritten, bytesWritten, buffer);
}

void MailSystem::DeleteReceivedPost(class CSocket *socket, const int userDbId, const int userObjectId, const std::vector<int> &ids)
{
	std::vector<int> deletedIds;
	for (std::vector<int>::const_iterator i = ids.begin() ; i != ids.end() ; ++i) {
		PostLocker::Guard lock(locker, *i);
		DBConn conn;
		int items[8];
		memset(items, 0, sizeof(items));
		conn.Prepare(
			L"SELECT p.item1, p.item2, p.item3, p.item4, p.item5, p.item6, p.item7, p.item8 "
			L"FROM dbo.post p "
			L"WHERE p.id=? "
			L"AND p.recipient_id=?");
		for (size_t j = 0 ; j < 8 ; ++j) conn.Bind(&items[j]);
		conn.BindParameter(*i);
		conn.BindParameter(userDbId);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't get post from DB");
			return;
		}
		if (!conn.Fetch()) continue;
		bool attachments = false;
		for (size_t j = 0 ; j < 8 ; ++j) {
			if (!items[j]) continue;
			attachments = true;
			break;
		}
		if (attachments) continue;
		conn.Unbind();
		conn.Prepare(L"UPDATE dbo.post SET recipient_deleted=1 WHERE id=?");
		conn.BindParameter(*i);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't update post");
			return;
		}
		deletedIds.push_back(*i);
	}
	socket->Send("chhddb", 0xD2, 0x0077, CDB::REPLY_DELETE_RECEIVED_POST,
		userObjectId, deletedIds.size(), deletedIds.size() * sizeof(deletedIds[0]), &deletedIds[0]);
}

void MailSystem::DeleteSentPost(class CSocket *socket, const int userDbId, const int userObjectId, const std::vector<int> &ids)
{
	std::vector<int> deletedIds;
	for (std::vector<int>::const_iterator i = ids.begin() ; i != ids.end() ; ++i) {
		PostLocker::Guard lock(locker, *i);
		DBConn conn;
		int id;
		conn.Prepare(
			L"SELECT p.id "
			L"FROM dbo.post p "
			L"WHERE p.id=? "
			L"AND p.sender_id=?");
		conn.Bind(&id);
		conn.BindParameter(*i);
		conn.BindParameter(userDbId);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't get post from DB");
			return;
		}
		if (!conn.Fetch()) continue;
		conn.Unbind();
		conn.Prepare(L"UPDATE dbo.post SET sender_deleted=1 WHERE id=?");
		conn.BindParameter(*i);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't update post");
			return;
		}
		deletedIds.push_back(id);
	}
	socket->Send("chhddb", 0xD2, 0x0077, CDB::REPLY_DELETE_SENT_POST,
		userObjectId, deletedIds.size(), deletedIds.size() * sizeof(deletedIds[0]), &deletedIds[0]);
}

void MailSystem::ReceivePost(class CSocket *socket, const int userDbId, const int userObjectId, const int id)
{
	GUARDED;

	PostLocker::Guard lock(locker, id);

	int senderDbId = 0;
	INT64 codAdena = 0;
	int items[8];
	memset(items, 0, sizeof(items));
	{
		DBConn conn;
		conn.Prepare(
			L"SELECT p.sender_id, p.cod_adena, "
			L"p.item1, p.item2, p.item3, p.item4, "
			L"p.item5, p.item6, p.item7, p.item8 "
			L"FROM dbo.post p "
			L"WHERE p.recipient_id=? "
			L"AND p.id=? "
			L"AND p.recipient_deleted=0");
		conn.Bind(&senderDbId);
		conn.Bind(&codAdena);
		for (size_t i = 0 ; i < 8 ; ++i) conn.Bind(&items[i]);
		conn.BindParameter(userDbId);
		conn.BindParameter(id);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't get post from DB");
			return;
		}
		if (!conn.Fetch()) {
			return;
		}
	}

	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t itemsWritten = 0;

	int realSenderDbId = senderDbId;
	if (!senderDbId) senderDbId = userDbId;

	SmartPtr<CUser> senderUser = CUser::GetUser(senderDbId, true, false);
	if (!senderUser) {
		CLog::Add(CLog::Red, L"Can't get user %d", senderDbId);
		return;
	}

	SmartPtr<CUser> recipientUser = CUser::GetUser(userDbId, true, false);
	if (!recipientUser) {
		CLog::Add(CLog::Red, L"Can't get user %d", userDbId);
		return;
	}

	SmartPtr<CWareHouse> mail = senderUser->GetWareHouse(CWareHouse::MAIL, true);
	if (!mail) {
		CLog::Add(CLog::Red, L"Can't get user %d mail warehouse", senderDbId);
		return;
	}

	SmartPtr<CWareHouse> inventory = recipientUser->GetWareHouse(CWareHouse::INVENTORY, true);
	if (!inventory) {
		CLog::Add(CLog::Red, L"Can't get user %d inventory warehouse", userDbId);
		return;
	}

	SmartPtr<CWareHouse> senderInventory;
	if (codAdena > 0) {
		senderInventory = senderUser->GetWareHouse(CWareHouse::INVENTORY, true);
		if (!senderInventory) {
			CLog::Add(CLog::Red, L"Can't get used %d inventory warehouse", senderDbId);
			return;
		}
	}
	int adenaItemDbId = 0;
	INT64 adena = 0;

	{
		CWareHouse::LockGuardN lock;
		lock.Add(&*mail);
		lock.Add(&*inventory);
		if (codAdena > 0) lock.Add(&*senderInventory);
		lock.Lock(__FILEW__, __LINE__);

		SmartPtr<CItem> adenaItem = inventory->GetAdena();
		if (codAdena > 0 && (!adenaItem || adenaItem->Amount() < codAdena)) {
			CLog::Add(CLog::Red, L"Not enough adena to pay payment request %ld < %ld", adenaItem->Amount(), codAdena);
			return;
		}

		CWareHouse::TransactionGuard transactionGuard;

		bool foundAdena = false;
		for (size_t i = 0 ; i < 8 ; ++i) {
			if (!items[i]) continue;
			SmartPtr<CItem> item = mail->GetItem(items[i]);
			if (!item) {
				CLog::Add(CLog::Red, L"Can't get user %d mail warehouse item %d", senderDbId, items[i]);
				return;
			}

			CWareHouse::TransactionGuard::Item &oldItem = transactionGuard.Update(item);
			SmartPtr<CItem> tmp;
			if (oldItem->IsStackable()) {
				tmp = inventory->GetItemByType(oldItem->ItemType());
			}
			if (tmp) {
				INT64 amount = oldItem->Amount();
				oldItem->SetAmount(0);
				oldItem->TransactDelete();
				CWareHouse::TransactionGuard::Item &updatedItem = transactionGuard.Update(tmp);
				updatedItem->SetAmount(updatedItem->Amount() + amount);
				oldItem.AddAudit(2003, &*senderUser, &*recipientUser, &*oldItem, -amount, 0);
				updatedItem.AddAudit(2004, &*recipientUser, &*senderUser, &*updatedItem, amount, updatedItem->Amount());
				if (adena > 0 && updatedItem->ItemType() == 57) {
					updatedItem->SetAmount(updatedItem->Amount() - adena);
					updatedItem.AddAudit(2003, &*recipientUser, &*senderUser, &*updatedItem, -adena, updatedItem->Amount());
					foundAdena = true;
				}
				item = updatedItem;
			} else {
				oldItem->SetOwnerDBID(userDbId);
				oldItem->SetWarehouse(CWareHouse::INVENTORY);
				oldItem.AddAudit(2003, &*senderUser, &*recipientUser, &*oldItem, -oldItem->Amount(), 0);
				oldItem.AddAudit(2004, &*recipientUser, &*senderUser, &*oldItem, oldItem->Amount(), oldItem->Amount());
				item = oldItem;
			}

			bytesWritten += Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "ddQdhhhhhhhh",
				item->ItemType(), item->GetDBID(), item->Amount(), item->enchant,
				item->attribute[0], item->attribute[1], item->attribute[2], item->attribute[3],
				item->attribute[4], item->attribute[5], item->attribute[6], item->attribute[7]);
			++itemsWritten;
		}

		if (codAdena > 0) {
			if (!foundAdena) {
				CWareHouse::TransactionGuard::Item &item = transactionGuard.Update(adenaItem);
				item->SetAmount(item->Amount() - codAdena);
				if (!item->Amount()) item->TransactDelete();
				item.AddAudit(2003, &*recipientUser, &*senderUser, &*item, -codAdena, item->Amount());
				bytesWritten += Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "ddQdhhhhhhhh",
					item->ItemType(), item->GetDBID(), item->Amount(), item->enchant,
					item->attribute[0], item->attribute[1], item->attribute[2], item->attribute[3],
					item->attribute[4], item->attribute[5], item->attribute[6], item->attribute[7]);
				++itemsWritten;
			}

			SmartPtr<CItem> tmp = senderInventory->GetAdena();
			CWareHouse::TransactionGuard::Item &item = tmp
				? transactionGuard.Update(tmp)
				: transactionGuard.Create(
					senderInventory->MakeNewItem(senderDbId, 57, codAdena, senderInventory->GetWarehouseNo()));
			if (tmp) item->SetAmount(item->Amount() + codAdena);
			item.AddAudit(2004, &*senderUser, &*recipientUser, &*item, codAdena, item->Amount());
			adenaItemDbId = item->GetDBID();
			adena = item->Amount();
		}

		int expireSeconds = Config::Instance()->mail->expireSeconds;

		{
			DBConn conn;
			conn.Prepare(
				L"UPDATE dbo.post SET item1=0, item2=0, item3=0, item4=0, "
				L"item5=0, item6=0, item7=0, item8=0, cod_adena=0, create_date=GETUTCDATE(), "
				L"expire_date=DATEADD(SECOND, ?, GETUTCDATE()), expired=0 WHERE id=?");
			conn.BindParameter(expireSeconds);
			conn.BindParameter(id);
			if (!conn.Execute()) {
				CLog::Add(CLog::Red, L"Can't update post items and adena");
				return;
			}
		}

		transactionGuard.Commit();
	}

	socket->Send("chhddddQQdb", 0xD2, 0x0077, CDB::REPLY_RECEIVE_POST,
		userObjectId, id, realSenderDbId, adenaItemDbId, adena, codAdena, itemsWritten, bytesWritten, buffer);
}

void MailSystem::CheckMail(class CSocket *socket, const int userDbId, const int userObjectId)
{
	GUARDED;

	int count = 0;
	DBConn conn;
	conn.Prepare(L"SELECT COUNT(*) FROM post WHERE recipient_id=? AND recipient_deleted=0 AND [read]=0");
	conn.Bind(&count);
	conn.BindParameter(userDbId);
	if (!conn.Execute() || !conn.Fetch()) {
		CLog::Add(CLog::Red, L"Can't get unread post count from DB");
		return;
	}
	socket->Send("chhdd", 0xD2, 0x0077, CDB::REPLY_CHECK_MAIL, userObjectId, count);
}

void MailSystem::RejectPost(class CSocket *socket, const int userDbId, const int userObjectId, const int id)
{
	GUARDED;

	PostLocker::Guard lock(locker, id);

	DBConn conn;
	conn.Prepare(
		L"SELECT p.sender_id "
		L"FROM dbo.post p "
		L"WHERE recipient_id=? "
		L"AND id=? "
		L"AND recipient_deleted=0 "
		L"AND sender_id!=0");
	int senderDbId = 0;
	conn.Bind(&senderDbId);
	conn.BindParameter(userDbId);
	conn.BindParameter(id);
	if (!conn.Execute()) {
		CLog::Add(CLog::Red, L"DB error");
		return;
	}
	if (!conn.Fetch()) {
		CLog::Add(CLog::Red, L"Post to reject not found in DB");
		return;
	}
	conn.Unbind();
	conn.Prepare(
		L"UPDATE dbo.post "
		L"SET recipient_id=sender_id, sender_id=0, sender_name=recipient_name, recipient_name=sender_name, "
		L"cod_adena=0, sender_deleted=1, [read]=0, subject='[Sent Back]' + SUBSTRING(subject, 1, 117), "
		L"expire_date=DATEADD(SECOND, ?, GETUTCDATE()), expired=0 "
		L"WHERE recipient_id=? "
		L"AND id=? "
		L"AND recipient_deleted=0 "
		L"AND sender_id=?");
	conn.BindParameter(Config::Instance()->mail->expireSeconds);
	conn.BindParameter(userDbId);
	conn.BindParameter(id);
	conn.BindParameter(senderDbId);
	if (!conn.Execute()) {
		CLog::Add(CLog::Red, L"DB error");
		return;
	}
	conn.Unbind();
	conn.Prepare(L"SELECT @@ROWCOUNT");
	int rows = 0;
	conn.Bind(&rows);
	if (!conn.Execute() || !conn.Fetch()) {
		CLog::Add(CLog::Red, L"DB error");
		return;
	}
	if (!rows) {
		CLog::Add(CLog::Red, L"Post to reject not found in DB");
		return;
	}
	socket->Send("chhddd", 0xD2, 0x0077, CDB::REPLY_REJECT_POST, userObjectId, id, senderDbId);
}

void MailSystem::ReturnPost(class CSocket *socket, const int senderDbId, const int recipientDbId, const int id, const std::vector<int> &items)
{
	GUARDED;

	if (!senderDbId) {
		do {
			SmartPtr<CUser> recipient = CUser::GetUser(recipientDbId, true, false);
			if (!recipient) {
				CLog::Add(CLog::Red, L"Upkeep: Can't get user %d", recipientDbId);
				break;
			}
			SmartPtr<CWareHouse> mail = recipient->GetWareHouse(CWareHouse::MAIL, true);
			if (!mail) {
				CLog::Add(CLog::Red, L"Upkeep: Can't get user %d mail warehouse", recipientDbId);
				break;
			}
			SmartPtr<CWareHouse> warehouse = recipient->GetWareHouse(CWareHouse::WAREHOUSE, true);
			if (!warehouse) {
				CLog::Add(CLog::Red, L"Upkeep: Can't get user %d warehouse/freight warehouse", recipientDbId);
				break;
			}
			{
				CWareHouse::LockGuard2 lock(&*mail, &*warehouse);

				lock.Lock(__FILEW__, __LINE__);

				CWareHouse::TransactionGuard transactionGuard;

				size_t warehouseItemCount = warehouse->itemCount;

				bool error = false;
				for (size_t i = 0 ; i < items.size() ; ++i) {
					SmartPtr<CItem> mailItem = mail->GetItem(items[i]);
					if (!mailItem) {
						CLog::Add(CLog::Red, L"Upkeep: Can't get item %d from user %d mail warehouse", items[i], recipientDbId);
						error = true;
						break;
					}
					SmartPtr<CItem> whItem;
					if (mailItem->IsStackable()) {
						whItem = warehouse->GetItemByType(mailItem->ItemType(), 0);
					}
					if (whItem) {
						CWareHouse::TransactionGuard::Item &oldItem = transactionGuard.Update(mailItem);
						CWareHouse::TransactionGuard::Item &newItem = transactionGuard.Update(whItem);
						INT64 amount = oldItem->Amount();
						oldItem->SetAmount(0);
						oldItem->TransactDelete();
						newItem->SetAmount(newItem->Amount() + amount);
						oldItem.AddAudit(2007, &*recipient, &*recipient, &*oldItem, -amount, 0);
						newItem.AddAudit(2008, &*recipient, &*recipient, &*newItem, amount, newItem->Amount());
					} else {
						if (++warehouseItemCount > 200) {
							CLog::Add(CLog::Red, L"[NO ERROR] Upkeep: user %d warehouse full", recipientDbId);
							error = true;
							break;
						}
						CWareHouse::TransactionGuard::Item &oldItem = transactionGuard.Update(mailItem);
						oldItem->SetWarehouse(CWareHouse::WAREHOUSE);
						oldItem.AddAudit(2007, &*recipient, &*recipient, &*oldItem, -oldItem->Amount(), 0);
						oldItem.AddAudit(2008, &*recipient, &*recipient, &*oldItem, oldItem->Amount(), oldItem->Amount());
					}
				}
				if (error) break;

				transactionGuard.Commit();

				DBConn conn;
				conn.Prepare(
					L"UPDATE dbo.post "
					L"SET "
					L"sender_deleted=1,"
					L"recipient_deleted=1,"
					L"item1=0, item2=0, item3=0, item4=0, item5=0, item6=0, item7=0, item8=0, "
					L"cod_adena=0, "
					L"expired=1 "
					L"WHERE id=?");
				conn.BindParameter(id);
				if (!conn.Execute()) {
					CLog::Add(CLog::Red, L"Upkeep: DB error [%s][%d]", __FILEW__, __LINE__);
					continue;
				}
			}
			return;
		} while (false);

		CLog::Add(CLog::Red, L"[NO ERROR] Upkeep: couldn't put items from post %d to user %d warehouse, will try again in 1 hour",
			id, recipientDbId);

		DBConn conn;
		conn.Prepare(
			L"UPDATE dbo.post "
			L"SET expire_date=DATEADD(HOUR, 1, GETUTCDATE()) "
			L"WHERE id=?");
		conn.BindParameter(id);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Upkeep: DB error [%s][%d]", __FILEW__, __LINE__);
			return;
		}
		return;
	}

	DBConn conn;
	conn.Prepare(
		L"UPDATE dbo.post "
		L"SET recipient_id=sender_id, sender_id=0, sender_name=recipient_name, recipient_name=sender_name, "
		L"cod_adena=0, sender_deleted=1, [read]=0, subject='[Sent Back]' + SUBSTRING(subject, 1, 117), "
		L"expire_date=DATEADD(SECOND, ?, GETUTCDATE()), expired=0 "
		L"WHERE id=?");
	conn.BindParameter(Config::Instance()->mail->expireSeconds);
	conn.BindParameter(id);
	if (!conn.Execute()) {
		CLog::Add(CLog::Red, L"Upkeep: DB error [%s][%d]", __FILEW__, __LINE__);
		return;
	}
	socket->Send("chhddd", 0xD2, 0x0077, CDB::REPLY_RETURNED_POST, senderDbId, recipientDbId, id);
}

void MailSystem::CancelPost(class CSocket *socket, const int userDbId, const int userObjectId, const int id)
{
	GUARDED;

	PostLocker::Guard lock(locker, id);

	int recipientDbId = 0;
	int read = 0;
	int items[8];
	memset(items, 0, sizeof(items));
	{
		DBConn conn;
		conn.Prepare(
			L"SELECT p.recipient_id, p.[read], "
			L"p.item1, p.item2, p.item3, p.item4, "
			L"p.item5, p.item6, p.item7, p.item8 "
			L"FROM dbo.post p "
			L"WHERE p.sender_id=? "
			L"AND p.id=? "
			L"AND p.sender_deleted=0");
		conn.Bind(&recipientDbId);
		conn.Bind(&read);
		for (size_t i = 0 ; i < 8 ; ++i) conn.Bind(&items[i]);
		conn.BindParameter(userDbId);
		conn.BindParameter(id);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't get post from DB");
			return;
		}
		if (!conn.Fetch()) {
			return;
		}
	}

	if (read) {
		socket->Send("chhdddd", 0xD2, 0x0077, CDB::REPLY_CANCEL_POST, userObjectId, 0, 0, 0);
		return;
	}

	char buffer[0x2000];
	size_t bytesWritten = 0;
	size_t itemsWritten = 0;

	SmartPtr<CUser> senderUser = CUser::GetUser(userDbId, true, false);
	if (!senderUser) {
		CLog::Add(CLog::Red, L"Can't get user %d", userDbId);
		return;
	}

	SmartPtr<CWareHouse> mail = senderUser->GetWareHouse(CWareHouse::MAIL, true);
	if (!mail) {
		CLog::Add(CLog::Red, L"Can't get user %d mail warehouse", userDbId);
		return;
	}

	SmartPtr<CWareHouse> inventory = senderUser->GetWareHouse(CWareHouse::INVENTORY, true);
	if (!inventory) {
		CLog::Add(CLog::Red, L"Can't get user %d inventory warehouse", userDbId);
		return;
	}

	{
		CWareHouse::LockGuard2 lock(&*mail, &*inventory);
		lock.Lock(__FILEW__, __LINE__);

		CWareHouse::TransactionGuard transactionGuard;

		for (size_t i = 0 ; i < 8 ; ++i) {
			if (!items[i]) continue;
			SmartPtr<CItem> item = mail->GetItem(items[i]);
			if (!item) {
				CLog::Add(CLog::Red, L"Can't get user %d mail warehouse item %d", userDbId, items[i]);
				return;
			}

			CWareHouse::TransactionGuard::Item &oldItem = transactionGuard.Update(item);
			SmartPtr<CItem> tmp;
			if (oldItem->IsStackable()) {
				tmp = inventory->GetItemByType(oldItem->ItemType());
			}
			if (tmp) {
				INT64 amount = oldItem->Amount();
				oldItem->SetAmount(0);
				oldItem->TransactDelete();
				CWareHouse::TransactionGuard::Item &updatedItem = transactionGuard.Update(tmp);
				updatedItem->SetAmount(updatedItem->Amount() + amount);
				oldItem.AddAudit(2005, &*senderUser, 0, &*oldItem, -amount, 0);
				updatedItem.AddAudit(2006, &*senderUser, 0, &*updatedItem, amount, updatedItem->Amount());
				item = updatedItem;
			} else {
				oldItem->SetOwnerDBID(userDbId);
				oldItem->SetWarehouse(CWareHouse::INVENTORY);
				oldItem.AddAudit(2005, &*senderUser, 0, &*oldItem, -oldItem->Amount(), 0);
				oldItem.AddAudit(2006, &*senderUser, 0, &*oldItem, oldItem->Amount(), oldItem->Amount());
				item = oldItem;
			}

			bytesWritten += Assemble(buffer + bytesWritten, sizeof(buffer) - bytesWritten, "ddQdhhhhhhhh",
				item->ItemType(), item->GetDBID(), item->Amount(), item->enchant,
				item->attribute[0], item->attribute[1], item->attribute[2], item->attribute[3],
				item->attribute[4], item->attribute[5], item->attribute[6], item->attribute[7]);
			++itemsWritten;
		}

		DBConn conn;
		conn.Prepare(
			L"UPDATE dbo.post SET item1=0, item2=0, item3=0, item4=0, "
			L"item5=0, item6=0, item7=0, item8=0, cod_adena=0, sender_deleted=1, recipient_deleted=1 WHERE id=?");
		conn.BindParameter(id);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Can't update post items and adena");
			return;
		}

		transactionGuard.Commit();
	}

	socket->Send("chhddddb", 0xD2, 0x0077, CDB::REPLY_CANCEL_POST,
		userObjectId, id, recipientDbId, itemsWritten, bytesWritten, buffer);
}

void MailSystem::Upkeep(class CSocket *socket)
{
	static bool alreadyRunning = false;
	static CriticalSection cs;
	{
		ScopedLock lock(cs);
		if (alreadyRunning) return;
		alreadyRunning = true;
	}
	for (size_t i = 0 ; i < Config::Instance()->mail->maxUpkeepBatchSize ; ++i) {
		DBConn conn;
		conn.Prepare(
			L"SELECT TOP 1 p.id "
			L"FROM dbo.post p "
			L"WHERE p.expired=0 "
			L"AND p.expire_date<GETUTCDATE() "
			L"ORDER BY p.expire_date");
		int id = 0;
		conn.Bind(&id);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Upkeep: DB error [%s][%d]", __FILEW__, __LINE__);
			continue;
		}
		if (!conn.Fetch()) {
			break;
		}

		PostLocker::Guard lock(locker, id);

		int senderDbId = 0;
		int recipientDbId = 0;
		int items[8];
		memset(items, 0, sizeof(items));

		conn.Unbind();
		conn.Prepare(
			L"SELECT p.sender_id, p.recipient_id, "
			L"p.item1, p.item2, p.item3, p.item4, p.item5, p.item6, p.item7, p.item8 "
			L"FROM dbo.post p "
			L"WHERE p.id=? "
			L"AND p.expired=0 "
			L"AND p.expire_date<GETUTCDATE()");
		conn.Bind(&senderDbId);
		conn.Bind(&recipientDbId);
		for (size_t j = 0 ; j < 8 ; ++j) conn.Bind(&items[j]);
		conn.BindParameter(id);
		if (!conn.Execute()) {
			CLog::Add(CLog::Red, L"Upkeep: DB error [%s][%d]", __FILEW__, __LINE__);
			continue;
		}
		if (!conn.Fetch()) {
			continue;
		}

		std::vector<int> itemVector;
		for (size_t j = 0 ; j < 8 ; ++j) {
			if (items[j]) itemVector.push_back(items[j]);
		}

		if (!itemVector.empty()) {
			ReturnPost(socket, senderDbId, recipientDbId, id, itemVector);
		} else {
			conn.Unbind();
			conn.Prepare(
				L"UPDATE dbo.post "
				L"SET "
				L"sender_deleted=1,"
				L"recipient_deleted=1,"
				L"expired=1 "
				L"WHERE id=?");
			conn.BindParameter(id);
			if (!conn.Execute()) {
				CLog::Add(CLog::Red, L"Upkeep: DB error [%s][%d]", __FILEW__, __LINE__);
				continue;
			}
		}
	}

	ScopedLock lock(cs);
	alreadyRunning = false;
}

} // namespace Cached

