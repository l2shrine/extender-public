
#pragma once

#include <Cached/CWareHouse.h>
#include <Cached/CObjectStorage.h>
#include <Common/CriticalSection.h>
#include <Common/SmartPtr.h>
#include <map>
#include <set>

namespace Cached {

class CUser {
public:
	class Ext {
	public:
		Ext();
		~Ext();

		inline CUser* GetUser()
		{
			return reinterpret_cast<CUser*>(reinterpret_cast<char*>(this) - offsetof(CUser, ext));
		}

		CObjectStorage<CWareHouse> mailWarehouse;
	};

	static std::map<UINT32, std::set<UINT32> > alreadyRefundedItems;
	static CriticalSection alreadyRefundedItemsCS;

	static void Init();

	static CUser* Constructor(CUser *self, wchar_t *a, wchar_t *b, unsigned int c, unsigned int d, unsigned int e,
		int f, int g, int h, int i, int j, int k, int l, int m, double n, double o, double p, double q, unsigned int r,
		INT64 s, int t, int u, unsigned int v, unsigned int w, unsigned int x, struct AbnormalStatus *y, int z,
		int aa, int ab, int ac, wchar_t *ad, unsigned char *ae, unsigned int af, unsigned int ag, unsigned int ah,
		unsigned int ai, unsigned int aj, int ak, int al, int am, int an, int ao, int ap, int aq, int ar, int as,
		int at, int au, int av, double aw, double ax, int ay, int az, int ba, int bb, int bc, int bd, int be,
		int bf, int bg, int bh, int bi, int bj, int bk, int bl, int bm, int bn, int bo, int bp, int bq, int br,
		int bs, int bt, int bu, int bv, int bw, int bx, int by, int bz, int ca, int cb, int cc);
	static CUser* Destructor(CUser *self, bool isMemoryFreeUsed);

	void WriteLogin(INT32 i);
	void WriteLogout(INT32 i, INT32 j);
	UINT32 GetId();
	SmartPtr<class CWareHouse> GetWareHouse(const int type, const bool load);
	int GetAccountID();
	int GetX();
	int GetY();
	int GetZ();
	int GetRace();
	int GetGender();
	int GetClass();
	int GetLevel();
	const wchar_t* GetCharName();
	const wchar_t* GetAccountName();

	void* BuyItemsPacketGetWarehouse(void *smartptr, int i, bool b);

	static SmartPtr<CUser> GetUser(const int dbId, const bool load, const bool mustExist);

	/* 0x0000 */ unsigned char padding0x0000[0x01D0 - 0x0000];
	/* 0x01D0 */ int dbId;
	/* 0x01D4 */ unsigned char padding0x01D4[0x09B0 - 0x01D4];
	/* 0x09B0 */ Ext ext;
};

} // namespace Cached

