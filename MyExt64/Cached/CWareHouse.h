
#pragma once

#include <Cached/CItem.h>
#include <Common/SmartPtr.h>
#include <list>
#include <map>

namespace Cached {

class CWareHouse {
public:
	class BuySellExt {
	public:
		BuySellExt() :
			refundId(0), bless(0), enchant(0), damaged(0),
			attributeAttackType(0), attributeAttackValue(0),
			attributeFire(0), attributeWater(0),
			attributeWind(0), attributeEarth(0),
			attributeDivine(0), attributeDark(0),
			attributeReserved(0), userId(0)
		{
		}

		UINT32 refundId;
		UINT32 bless;
		UINT32 enchant;
		UINT32 damaged;
		UINT16 attributeAttackType;
		UINT16 attributeAttackValue;
		UINT16 attributeFire;
		UINT16 attributeWater;
		UINT16 attributeWind;
		UINT16 attributeEarth;
		UINT16 attributeDivine;
		UINT16 attributeDark;
		UINT16 attributeReserved;
		UINT32 userId;
	};

	class LockGuard {
	public:
		LockGuard(CWareHouse *warehouse, const bool writeLock = true);
		~LockGuard();
		void Lock(const wchar_t *file, const int line);
		void Unlock();

	protected:
		const bool writeLock;
		CWareHouse *warehouse;
		bool locked;
	};

	class LockGuardN {
	public:
		LockGuardN();
		~LockGuardN();
		void Add(CWareHouse *warehouse);
		void Lock(const wchar_t *file, const int line);
		void Unlock();

	protected:
		std::map<UINT64, CWareHouse*> warehouses;
		bool locked;
	};

	class LockGuard2 : public LockGuardN {
	public:
		LockGuard2(CWareHouse *warehouse1, CWareHouse *warehouse2);
	};

	class LockGuard3 : public LockGuardN {
	public:
		LockGuard3(CWareHouse *warehouse1, CWareHouse *warehouse2, CWareHouse *warehouse3);
	};

	class TransactionGuard {
	public:
		class Item {
		protected:
			Item(const Item &other) { }
			Item& operator=(const Item &other) { return *this; }

		public:
			class AuditInfo {
			public:
				int eventId;
				class CUser *user1;
				class CUser *user2;
				class CItem *item;
				INT64 change;
				INT64 newAmount;

				void Log() const;
			};

			Item(TransactionGuard &guard, SmartPtr<CItem> item, CItem::TransactionMode mode);
			CItem* operator->();
			CItem& operator*();
			operator bool();
			operator SmartPtr<CItem>();
			void AddAudit(int eventId, CUser *user1, CUser *user2, CItem *item, INT64 change, INT64 newAmount);
			void Log() const;

			SmartPtr<CItem> original;
			SmartPtr<CItem> backup;
			std::list<AuditInfo> auditInfo;
		};

		TransactionGuard();
		~TransactionGuard();
		Item& Create(SmartPtr<CItem> item);
		Item& Update(SmartPtr<CItem> item);
		void Commit();
		void Rollback();

	protected:
		bool done;
		std::list<Item*> transactions;
	};

	enum Type {
		INVENTORY = 0,
		WAREHOUSE = 1,
		FREIGHT = 1,
		PLEDGE = 2,
		CASTLE = 3,
		MANOR = 4,
		PET = 5,
		MAIL = 6
	};

	static BuySellExt buySellExt[24];

    static void Init();
	virtual ~CWareHouse() { }
	virtual void AddRef(const wchar_t *file, const int line, const int unknown) { }
	virtual void Release(const wchar_t *file, const int line, const int unknown) { }
	void Initialize(const int warehouseNo, bool loadItems);
	SmartPtr<CItem> GetItem(const int itemId);
	UINT64 GetLockAddr();
	void ReadLock(const wchar_t *file, const int line);
	void ReadUnlock();
	void WriteLock(const wchar_t *file, const int line);
	void WriteUnlock();
	bool AddAdena(INT64 amount);
	bool DelAdena(INT64 amount);
	bool PushItem(CItem *item);
	SmartPtr<CItem> PopItem(int id);
	SmartPtr<CItem> MakeNewItem(int ownerId, int itemType, INT64 amount, int warehouseNo);
	int GetWarehouseNo() const;
	void LoadItems(const int warehouseNo);
	SmartPtr<CItem> GetAdena();
	SmartPtr<CItem> GetItemByType(const int type, const int ident = 0);

	/* 0x0008 */ unsigned char padding0x0008[0x00A0 - 0x0008];
	/* 0x00A0 */ int itemCount;
	/* 0x00A4 */ int ownerDBID;
	/* 0x00A8 */ bool loaded;
	/* 0x00AC */ int warehouseNo;
	/* 0x00B0 */ unsigned char padding0x00B0[0x00B8 - 0x00B0];

protected:
	static int CWareHouse::AssembleSoldItem(char *buffer, int limit, void *rsp, int itemId, INT64 newAmount);
	static const unsigned char* CWareHouse::DisassembleSoldItem(const unsigned char *buffer, void *rsp, int *itemId, INT64 *amount);
	static const unsigned char* BuyItemsDisassembleItem(const unsigned char *buffer, char *rsp, int *itemId, INT64 *amount, int *consumeType);
	static int BuyItemsAssembleItem(char *buffer, int limit, char *rsp, UINT32 itemId, UINT32 itemTypeId, UINT64 amount, UINT32 enchant, UINT32 damaged, UINT32 bless, UINT32 ident, UINT32 wished, UINT32 unknown);
	static void* BuyItemsMakeNewItem(
		CWareHouse *self, char *ptr, UINT32 userId, UINT32 itemType, INT64 amount,
		UINT32 enchant, UINT32 damaged, UINT32 bless, UINT32 unknown1, UINT32 unknown2, UINT32 warehouseNo,
		UINT32 *option, UINT32 unknown3, UINT16 *attribute);
	static UINT64 BuyItemsHelper(UINT64 adenaPtr, INT64 price, INT32 *adenaItemId);
};

} // namespace Cached

